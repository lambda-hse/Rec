/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloTrackMatchAlg.h"

// ============================================================================
/** @class PhotonMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// ============================================================================
using TABLE     = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
using CALOTYPES = LHCb::CaloClusters;

struct PhotonMatchAlg final : CaloTrackMatchAlg<TABLE, CALOTYPES> {
  static_assert( std::is_base_of<LHCb::Calo2Track::IClusTrTable2D, TABLE>::value,
                 "TABLE must inherit from IClusTrTable2D" );
  PhotonMatchAlg( const std::string& name, ISvcLocator* pSvc ) : CaloTrackMatchAlg<TABLE, CALOTYPES>( name, pSvc ) {
    updateHandleLocation( *this, "Calos", LHCb::CaloAlgUtils::CaloClusterLocation( "Ecal", context() ) );
    updateHandleLocation( *this, "Output", LHCb::CaloAlgUtils::CaloIdLocation( "ClusterMatch", context() ) );
    updateHandleLocation( *this, "Filter", LHCb::CaloAlgUtils::CaloIdLocation( "InEcal", context() ) );

    _setProperty( "Tool", "CaloPhotonMatch/PhotonMatch" );
    _setProperty( "Threshold", "1000" );
    // track types:
    _setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                                               LHCb::Track::Types::Ttrack ) );
    _setProperty( "TableSize", "5000" );
  }
};

// ============================================================================

DECLARE_COMPONENT( PhotonMatchAlg )
