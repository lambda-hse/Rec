/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <vector>

// LHCb
#include "Event/PrVeloHits.h"
#include "Event/PrVeloTracks.h"

/**
 * Velo only Kalman fit helpers
 */

namespace VeloKalmanParam {
  constexpr float err = 0.0158771324f; // 0.055f / sqrt( 12 ); //TODO: find a solution so this compile with clang
  constexpr float wx  = err * err;
  constexpr float wy  = wx;
} // namespace VeloKalmanParam

template <typename T>
class FittedState {
public:
  T x, y, z;
  T tx, ty;
  T covXX, covXTx, covTxTx;
  T covYY, covYTy, covTyTy;

  FittedState() {}

  FittedState( Vec3<T> pos, Vec3<T> dir, T covXX, T covXTx, T covTxTx, T covYY, T covYTy, T covTyTy )
      : x( pos.x )
      , y( pos.y )
      , z( pos.z )
      , tx( dir.x )
      , ty( dir.y )
      , covXX( covXX )
      , covXTx( covXTx )
      , covTxTx( covTxTx )
      , covYY( covYY )
      , covYTy( covYTy )
      , covTyTy( covTyTy ) {}

  inline Vec3<T> pos() const { return Vec3<T>( x, y, z ); }
  inline Vec3<T> dir() const { return Vec3<T>( tx, ty, 1.f ); }
  inline Vec3<T> covX() const { return Vec3<T>( covXX, covXTx, covTxTx ); }
  inline Vec3<T> covY() const { return Vec3<T>( covYY, covYTy, covTyTy ); }

  inline T zBeam() const {
    const T x0    = x - z * tx;
    const T y0    = y - z * ty;
    T       denom = tx * tx + ty * ty;
    return select( denom < 0.001f * 0.001f, z, -( x0 * tx + y0 * ty ) / denom );
  }

  inline void transportTo( const T& toZ ) {
    const T dz  = toZ - z;
    const T dz2 = dz * dz;

    x = x + dz * tx;
    y = y + dz * ty;
    z = toZ;

    covXX  = covXX + dz2 * covTxTx + 2.f * dz * covXTx;
    covXTx = covXTx + dz * covTxTx;
    covYY  = covYY + dz2 * covTyTy + 2.f * dz * covYTy;
    covYTy = covYTy + dz * covTyTy;
  }
};

template <typename M, typename F>
inline void filter( const M& mask, const F& z, F& x, F& tx, F& covXX, F& covXTx, F& covTxTx, const F& zhit,
                    const F& xhit, const F& winv ) {
  // compute prediction
  const F dz    = zhit - z;
  const F predx = x + dz * tx;

  const F dz_t_covTxTx = dz * covTxTx;
  const F predcovXTx   = covXTx + dz_t_covTxTx;
  const F dz_t_covXTx  = dz * covXTx;

  const F predcovXX   = covXX + 2.f * dz_t_covXTx + dz * dz_t_covTxTx;
  const F predcovTxTx = covTxTx;

  // compute the gain matrix
  const F R   = 1.0f / ( winv + predcovXX );
  const F Kx  = predcovXX * R;
  const F KTx = predcovXTx * R;

  // update the state vector
  const F r = xhit - predx;
  x         = select( mask, predx + Kx * r, x );
  tx        = select( mask, tx + KTx * r, tx );

  // update the covariance matrix
  covXX   = select( mask, ( 1.f - Kx ) * predcovXX, covXX );
  covXTx  = select( mask, ( 1.f - Kx ) * predcovXTx, covXTx );
  covTxTx = select( mask, predcovTxTx - KTx * predcovXTx, covTxTx );
}

template <typename F, typename I, typename M>
inline FittedState<F> fitBackward( const M& track_mask, LHCb::Pr::Velo::Tracks& tracks, int t,
                                   const LHCb::Pr::Velo::Hits& hits, const int state_id ) {
  I       nHits   = tracks.nHits<I>( t );
  int     maxHits = nHits.hmax( track_mask );
  I       idxHit0 = tracks.hit<I>( t, 0 );
  Vec3<F> dir     = tracks.stateDir<F>( t, state_id );
  Vec3<F> pos     = hits.maskgather_pos<F>( idxHit0, track_mask, 0.f );

  FittedState<F> s = FittedState<F>( pos, dir, 100.f, 0.f, 0.0001f, 100.f, 0.f, 0.0001f );

  // Parameters for kalmanfit scattering. calibrated on MC, shamelessly hardcoded:
  const F noise2PerLayer = 1e-8f + 7e-6f * ( s.tx * s.tx + s.ty * s.ty );

  for ( int i = 1; i < maxHits; i++ ) {
    auto    mask   = track_mask && ( I( i ) < nHits );
    I       idxHit = tracks.hit<I>( t, i );
    Vec3<F> hit    = hits.maskgather_pos<F>( idxHit, mask, 0.f );

    s.covTxTx = select( mask, s.covTxTx + noise2PerLayer, s.covTxTx );
    s.covTyTy = select( mask, s.covTyTy + noise2PerLayer, s.covTyTy );

    filter( mask, s.z, s.x, s.tx, s.covXX, s.covXTx, s.covTxTx, hit.z, hit.x, F( VeloKalmanParam::wx ) );
    filter( mask, s.z, s.y, s.ty, s.covYY, s.covYTy, s.covTyTy, hit.z, hit.y, F( VeloKalmanParam::wy ) );
    s.z = select( mask, hit.z, s.z );
  }

  s.covTxTx = s.covTxTx + noise2PerLayer;
  s.covTyTy = s.covTyTy + noise2PerLayer;

  return s;
}

template <typename F, typename I, typename M>
inline FittedState<F> fitForward( const M& track_mask, LHCb::Pr::Velo::Tracks& tracks, int t,
                                  const LHCb::Pr::Velo::Hits& hits, const int state_id ) {
  I       nHits   = tracks.nHits<I>( t );
  int     maxHits = nHits.hmax( track_mask );
  auto    mask    = track_mask && I( maxHits - 1 ) < nHits;
  I       idxHit0 = tracks.hit<I>( t, maxHits - 1 );
  Vec3<F> dir     = tracks.stateDir<F>( t, state_id );
  Vec3<F> pos     = hits.maskgather_pos<F>( idxHit0, mask, 0.f );

  FittedState<F> s = FittedState<F>( pos, dir, 100.f, 0.f, 0.0001f, 100.f, 0.f, 0.0001f );

  // Parameters for kalmanfit scattering. calibrated on MC, shamelessly hardcoded:
  const F noise2PerLayer = 1e-8f + 7e-6f * ( s.tx * s.tx + s.ty * s.ty );

  for ( int i = maxHits - 2; i >= 0; i-- ) {
    auto    mask   = track_mask && ( I( i ) < nHits );
    I       idxHit = tracks.hit<I>( t, i );
    Vec3<F> hit    = hits.maskgather_pos<F>( idxHit, mask, 0.f );

    s.covTxTx = select( mask, s.covTxTx + noise2PerLayer, s.covTxTx );
    s.covTyTy = select( mask, s.covTyTy + noise2PerLayer, s.covTyTy );

    filter( mask, s.z, s.x, s.tx, s.covXX, s.covXTx, s.covTxTx, hit.z, hit.x, F( VeloKalmanParam::wx ) );
    filter( mask, s.z, s.y, s.ty, s.covYY, s.covYTy, s.covTyTy, hit.z, hit.y, F( VeloKalmanParam::wy ) );
    s.z = select( mask, hit.z, s.z );
  }

  s.covTxTx = s.covTxTx + noise2PerLayer;
  s.covTyTy = s.covTyTy + noise2PerLayer;

  return s;
}
