/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOFUTUREALGS_CLASSIFYPHOTONELECTRONALG_H
#define CALOFUTUREALGS_CLASSIFYPHOTONELECTRONALG_H 1
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "ICaloFutureHypoTool.h"
#include "Relations/RelationWeighted2D.h"

#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include <DetDesc/ConditionAccessorHolder.h>

#include <string>

/** @class ClassifyPhotonElectronAlg ClassifyPhotonElectronAlg.h
 *
 *  Classification of electromagnetic clusters into photon and
 *  electron hypothesis according to track matching chi2.
 *  Energy and position corrections are applied to the resulting
 *  calo hypotheses.
 *  Implementation partially based on previous SinglePhotonAlg
 *  and ElectronAlg
 *
 *  @author Carla Marin carla.marin@cern.ch
 *  @date   23/05/2019
 */

namespace LHCb::Calo::Algorithm {
  using TrackMatchTable = RelationWeighted2D<CaloCluster, Track, float>;

  class ClassifyPhotonElectronAlg
      : public Gaudi::Functional::MultiTransformer<
            std::tuple<CaloHypos, CaloHypos>( const DeCalorimeter&, const CaloClusters&, const TrackMatchTable& ),
            DetDesc::usesConditions<DeCalorimeter>> {
  public:
    ClassifyPhotonElectronAlg( const std::string& name, ISvcLocator* pSvc );

    // returns 2 CaloHypos: photons and electrons respectively
    std::tuple<CaloHypos, CaloHypos> operator()( const DeCalorimeter&, const CaloClusters&,
                                                 const TrackMatchTable& ) const override;

  private:
    void printDebugInfo( const CaloCluster*                                       cluster,
                         CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>& eT ) const;
    bool validateCluster( const CaloCluster*                                       cluster,
                          CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>& eT ) const;
    void printHypoDebugInfo( CaloHypo* hypo, const bool& pass ) const;

    // Correction tools for now used as they were in
    // SinglePhotonAlg and ElectronAlg
    PublicToolHandleArray<Interfaces::IProcessHypos> m_correc_photon{this,
                                                                     "PhotonCorrectionTools",
                                                                     {
                                                                         "CaloFutureECorrection/ECorrectionPhoton",
                                                                         "CaloFutureSCorrection/SCorrectionPhoton",
                                                                         "CaloFutureLCorrection/LCorrectionPhoton",
                                                                     },
                                                                     "List of tools for 'fine-corrections' "};
    PublicToolHandleArray<Interfaces::IProcessHypos> m_correc_electr{this,
                                                                     "ElectronCorrectionTools",
                                                                     {
                                                                         "CaloFutureECorrection/ECorrectionElectron",
                                                                         "CaloFutureSCorrection/SCorrectionElectron",
                                                                         "CaloFutureLCorrection/LCorrectionElectron",
                                                                     },
                                                                     "List of tools for 'fine-corrections' "};

    // selection cuts
    Gaudi::Property<float>  m_ecut{this, "MinEnergy", 0., "Threshold on cluster energy"};
    Gaudi::Property<float>  m_eTcut{this, "MinET", 0., "Threshold on cluster transverse energy"};
    Gaudi::Property<int>    m_minDigits{this, "MinDigits", 0, "Threshold on minimum cluster digits"};
    Gaudi::Property<int>    m_maxDigits{this, "MaxDigits", 9999, "Threshold on maximum cluster digits"};
    Gaudi::Property<double> m_photonEtCut{this, "PhotonMinEt", 0., "Threshold on photon cluster & hypo ET"};
    Gaudi::Property<double> m_electrEtCut{this, "ElectrMinEt", 0., "Threshold on electron cluster & hypo ET"};
    Gaudi::Property<float>  m_photonChi2Cut{this, "PhotonMinChi2", 0.,
                                           "Threshold on minimum photon cluster track match chi2"};
    Gaudi::Property<float>  m_electrChi2Cut{this, "ElectrMaxChi2", 0.,
                                           "Threshold on maximum electron cluster track match chi2"};

    mutable Gaudi::Accumulators::StatCounter<>          m_counterPhotons{this, "photonHypos"};
    mutable Gaudi::Accumulators::StatCounter<>          m_counterElectrs{this, "electronHypos"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_errApply{this,
                                                                   "Error from Correction Tool - skip the cluster", 3};
  };
} // namespace LHCb::Calo::Algorithm

// ============================================================================
#endif // CLASSIFYPHOTONELECTRONALG_H
