/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// CaloFutureInterfaces
#include "CaloFutureInterfaces/ICaloFutureLikelihood.h"
// local
#include "CaloFutureSelector.h"

// ============================================================================
/** @file CaloFutureSelector.cpp
 *
 *  Implementation file for class : CaloFutureSelector
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 31/03/2002
 */
// ============================================================================

DECLARE_COMPONENT( CaloFutureSelector )

// ============================================================================
/** "select"/"preselect" method (functor interface)
 *  @see ICaloFutureClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelector::operator()( const LHCb::CaloCluster* cluster ) const {
  return cluster && m_cut <= ( *m_likelihood )( cluster );
}

// ============================================================================
// The End
// ============================================================================
