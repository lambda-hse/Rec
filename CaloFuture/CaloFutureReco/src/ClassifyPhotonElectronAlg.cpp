/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#include "ClassifyPhotonElectronAlg.h"

// ============================================================================
/** @file
 *
 *  Implementation file for class: ClassifyPhotonElectronAlg
 *  The implementation is partially based on previous
 *  SinglePhotonAlg and ElectronAlg codes.
 *
 *  @author Carla Marin carla.marin@cern.ch
 *  @date   23/05/2019
 */
// ============================================================================

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::Algorithm::ClassifyPhotonElectronAlg, "ClassifyPhotonElectronAlg" )

namespace LHCb::Calo::Algorithm {

  namespace {
    bool apply( const ToolHandleArray<Interfaces::IProcessHypos>& c, span<LHCb::CaloHypo* const> hypos,
                const TrackMatchTable* ctable = nullptr ) {
      return std::all_of( std::begin( c ), std::end( c ),
                          [&]( const auto& elem ) { return elem->correct( hypos, ctable ).isSuccess(); } );
    }
  } // namespace

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================
  ClassifyPhotonElectronAlg::ClassifyPhotonElectronAlg( const std::string& name, ISvcLocator* pSvc )
      : MultiTransformer( name, pSvc,
                          // Inputs
                          {
                              KeyValue( "Detector", {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )} ),
                              KeyValue( "InputClusters", {CaloClusterLocation::Ecal} ),
                              KeyValue( "InputTable", {CaloFutureIdLocation::ClusterMatch} ),
                          },
                          // Outputs
                          {KeyValue( "OutputPhotons", {CaloHypoLocation::Photons} ),
                           KeyValue( "OutputElectrons", {CaloHypoLocation::Electrons} )} ) {}

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  std::tuple<CaloHypos, CaloHypos> ClassifyPhotonElectronAlg::
                                   operator()( const DeCalorimeter& calo, const CaloClusters& clusters, const TrackMatchTable& table ) const {
    // output containers
    auto result                = std::tuple<CaloHypos, CaloHypos>{};
    auto& [photons, electrons] = result;
    photons.reserve( clusters.size() );
    electrons.reserve( clusters.size() );

    // used in the loop
    auto eT = CaloDataFunctor::EnergyTransverse{&calo};

    auto hasTrackMatch = [&table]( const CaloCluster& cluster, double chi2Cut ) {
      return !( table.relations( &cluster, chi2Cut, false ) ).empty();
    };

    // loop on clusters
    for ( const auto* cl : clusters ) {
      if ( msgLevel( MSG::DEBUG ) ) printDebugInfo( cl, eT );
      if ( validateCluster( cl, eT ) ) {
        // 1. check if cluster fullfills photon hypo requirements
        if ( ( CaloMomentum( cl ).pt() >= m_photonEtCut ) && !hasTrackMatch( *cl, m_photonChi2Cut ) ) {
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << " --> Cluster satisfies photon e, et, digits, pt and chi2 requirements" << endmsg;
          }

          auto photonHypo = std::make_unique<CaloHypo>();
          photonHypo->setHypothesis( CaloHypo::Hypothesis::Photon );
          photonHypo->addToClusters( cl );
          photonHypo->setPosition( std::make_unique<CaloPosition>( cl->position() ) );
          photons.insert( photonHypo.release() );
        }

        // 2. check if cluster fullfills electron hypo requirements
        if ( ( CaloMomentum( cl ).pt() >= m_electrEtCut ) && hasTrackMatch( *cl, m_electrChi2Cut ) ) {
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << " --> Cluster satisfies electron e, et, digits, pt and chi2 requirements" << endmsg;
          }

          auto electronHypo = std::make_unique<CaloHypo>();
          electronHypo->setHypothesis( CaloHypo::Hypothesis::EmCharged );
          electronHypo->addToClusters( cl );
          electronHypo->setPosition( std::make_unique<CaloPosition>( cl->position() ) );
          electrons.insert( electronHypo.release() );
        }
      }
    }

    bool corr_photons_ok = apply( m_correc_photon, make_span( photons.begin(), photons.end() ) );
    bool corr_electrons_ok = apply( m_correc_electr, make_span( electrons.begin(), electrons.end() ), &table );
    if ( UNLIKELY( !corr_photons_ok ) || UNLIKELY( !corr_electrons_ok ) ) ++m_errApply;

    photons.erase( std::remove_if( photons.begin(), photons.end(),
                                   [&]( const auto& hypo ) {
                                     bool pass = CaloMomentum( hypo ).pt() >= m_eTcut;
                                     if ( msgLevel( MSG::DEBUG ) ) printHypoDebugInfo( hypo, pass );
                                     return !pass;
                                   } ),
                   photons.end() );
    electrons.erase( std::remove_if( electrons.begin(), electrons.end(),
                                     [&]( const auto& hypo ) {
                                       bool pass = CaloMomentum( hypo ).pt() >= m_electrEtCut;
                                       if ( msgLevel( MSG::DEBUG ) ) printHypoDebugInfo( hypo, pass );
                                       return !pass;
                                     } ),
                     electrons.end() );

    // debug info
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " # of created Photon Hypos is " << photons.size() << endmsg;
      debug() << " # of created Electron Hypos is " << electrons.size() << endmsg;
    }

    // counters
    m_counterPhotons += photons.size();
    m_counterElectrs += electrons.size();

    return result;
  }

  void ClassifyPhotonElectronAlg::printDebugInfo( const CaloCluster*                                       cluster,
                                                  CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>& eT ) const {
    debug() << "*Variables and cut values:" << endmsg;
    debug() << " - e: " << cluster->e() << " " << m_ecut << endmsg;
    debug() << " - eT:" << eT( cluster ) << " " << m_eTcut << endmsg;
    debug() << " - pt:" << CaloMomentum( cluster ).pt() << " " << m_photonEtCut << endmsg;
    debug() << " - m: " << cluster->entries().size() << " " << m_minDigits << " " << m_maxDigits << endmsg;
    debug() << " - chi2 cut: " << m_photonChi2Cut << endmsg;
  }

  void ClassifyPhotonElectronAlg::printHypoDebugInfo( CaloHypo* const hypo, const bool& pass ) const {
    debug() << " - pt hypo: " << CaloMomentum( hypo ).pt() << endmsg;
    debug() << " - pt cut: " << m_eTcut << endmsg;
    if ( !pass ) debug() << "DOES NOT PASS!" << endmsg;
  }

  bool ClassifyPhotonElectronAlg::validateCluster( const CaloCluster*                                       cluster,
                                                   CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>& eT ) const {
    int m = cluster->entries().size();
    return ( cluster->e() > m_ecut ) && ( eT( cluster ) > m_eTcut ) && ( m > m_minDigits ) && ( m < m_maxDigits );
  }

} // namespace LHCb::Calo::Algorithm
