/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "CaloFutureCorrectionBase.h"
#include "Event/ProtoParticle.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureCorrectionBase
//
// 2010-05-07 : Olivier Deschamps
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( CaloFutureCorrectionBase )

CaloFutureCorrectionBase::CaloFutureCorrectionBase( const std::string& type, const std::string& name,
                                                    const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<CaloFutureCorrectionBase>( this );
  m_cmLoc = LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" );

  // allocate memory for m_params. params[area][type]
  CaloFutureCorrection::ParamVector             vect;
  CaloFutureCorrection::Parameters              typePar_pair = std::make_pair( CaloFutureCorrection::Empty, vect );
  std::vector<CaloFutureCorrection::Parameters> vect_pair( CaloFutureCorrection::nT, typePar_pair );
  m_params.reserve( area_size ); // area_size=3 (outer, middle, inner)
  for ( auto area = 0; area < area_size; ++area ) m_params.push_back( vect_pair );
}

StatusCode CaloFutureCorrectionBase::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialize" << endmsg;

  // transform vector of accepted hypos
  m_hypos.clear();
  for ( const auto& hypo : m_hypos_ ) {
    if ( hypo <= (int)LHCb::CaloHypo::Hypothesis::Undefined || hypo >= (int)LHCb::CaloHypo::Hypothesis::Other ) {
      return Error( "Invalid/Unknown  Calorimeter hypothesis object!" );
    }
    m_hypos.push_back( LHCb::CaloHypo::Hypothesis( hypo ) );
  }

  // locate and set and configure the Detector
  m_det = getDet<DeCalorimeter>( m_detData );
  if ( !m_det ) { return StatusCode::FAILURE; }
  m_calo.setCaloFuture( m_detData );
  //
  if ( m_hypos.empty() ) return Error( "Empty vector of allowed Calorimeter Hypotheses!" );

  // debug printout of all allowed hypos
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << " List of allowed hypotheses : " << endmsg;
    for ( const auto& h : m_hypos ) { debug() << " -->" << h << endmsg; }
    for ( const auto& c : m_corrections ) { debug() << "Accepted corrections :  '" << c << "'" << endmsg; }
  }

  // get external tools
  m_caloElectron = tool<LHCb::Calo::Interfaces::IElectron>( "CaloFutureElectron", this );
  counterStat    = tool<IFutureCounterLevel>( "FutureCounterLevel" );
  return setConditionParams( m_conditionName );
}

StatusCode CaloFutureCorrectionBase::finalize() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Finalize" << endmsg;

  if ( m_corrections.size() > 1 || *( m_corrections.begin() ) != "All" ) {
    for ( const auto& c : m_corrections ) { info() << "Accepted corrections :  '" << c << "'" << endmsg; }
  }
  if ( m_corrections.empty() ) warning() << "All corrections have been disabled for " << name() << endmsg;

  if ( m_cond == nullptr )
    warning() << " Applied corrections configured via options for  " << name() << endmsg;
  else if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << " Applied corrections configured via condDB  ('" << m_conditionName << "') for " << name() << endmsg;

  for ( auto area = 0; area < 3; ++area ) {
    for ( auto itype = 0; itype < static_cast<int>( CaloFutureCorrection::nT ); ++itype ) {
      int func = m_params[area][itype].first;
      if ( func == CaloFutureCorrection::Empty ) continue;
      const auto& type = CaloFutureCorrection::typeName[itype];
      const auto& vec  = m_params[area][itype].second;

      if ( !vec.empty() ) {

        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
          debug() << " o  '" << type << "'  correction as a '" << CaloFutureCorrection::funcName[func]
                  << "' function of " << vec.size() << " parameters" << endmsg;
        }
      } else {
        warning() << " o '" << type << "' correction HAS NOT BEEN APPLIED  (badly configured)" << endmsg;
      }
    }
  }

  m_hypos.clear();

  return GaudiTool::finalize(); // must be called after all other actions
}

//=============================================================================
StatusCode CaloFutureCorrectionBase::setDBParams() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Get params from CondDB condition = " << m_conditionName << endmsg;
  registerCondition( m_conditionName, m_cond, &CaloFutureCorrectionBase::updParams );
  return runUpdate();
}
// ============================================================================
StatusCode CaloFutureCorrectionBase::setOptParams() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Get params from options " << endmsg;
  if ( m_optParams.empty() && m_conditionName != "none" ) {
    info() << "No default options parameters defined" << endmsg;
    return StatusCode::SUCCESS;
  }
  for ( const auto& p : m_optParams ) {
    const std::string& name = p.first;
    if ( accept( name ) ) { Params( name, p.second ); }
  }
  checkParams();
  return StatusCode::SUCCESS;
}
// ============================================================================
StatusCode CaloFutureCorrectionBase::updParams() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "updParams() called" << endmsg;
  if ( !m_cond ) return Error( "Condition points to NULL", StatusCode::FAILURE );

  // toDo
  for ( const auto& paramName : m_cond->paramNames() ) {
    if ( m_cond->exists( paramName ) ) {
      const auto& params = m_cond->paramAsDoubleVect( paramName );
      if ( accept( paramName ) ) { Params( paramName, params ); }
    }
  }
  checkParams();

  return StatusCode::SUCCESS;
}
void CaloFutureCorrectionBase::Params( const std::string& paramName, const CaloFutureCorrection::ParamVector& params ) {
  // check if known type
  CaloFutureCorrection::Type type = stringToCorrectionType( paramName );
  bool                       ok   = false;
  for ( int itype = 0; itype < static_cast<int>( CaloFutureCorrection::lastType ); ++itype ) {
    if ( static_cast<int>( type ) == itype ) {
      ok = true;
      break;
    }
  }
  if ( !ok ) {
    warning() << " o Type " << paramName << " is not registered" << endmsg;
    return;
  }

  // const auto& active_par = Params( paramName, params );
  // if ( !active_par.active ) continue;

  // get parameters
  // const auto& pars = active_par.data;
  if ( params.size() < 2 ) return;
  for ( int area = 0; area <= 2; area++ ) {

    // consistency of pars checked elsewhere - straight parsing here
    const auto& func         = params[0];
    const auto& dim          = params[1];
    const int   step         = ( func != CaloFutureCorrection::GlobalParamList ) ? 3 : 1;
    const int   start_offset = ( func != CaloFutureCorrection::GlobalParamList ) ? area : 0;
    int         pos          = 2 + start_offset;

    if ( step * dim + 2 != static_cast<int>( params.size() ) ) {
      warning() << "o Size of DB parameter vector does not match the nominal value. : [ " << params << " ]" << endmsg;
      continue;
    }

    m_params[area][type].first = static_cast<CaloFutureCorrection::Function>( func );

    m_params[area][type].second.clear();
    m_params[area][type].second.reserve( dim );
    for ( int i = 0; i < dim; ++i ) {
      m_params[area][type].second.push_back( params[pos] );
      pos += step;
    }
  }
}

const CaloFutureCorrection::Parameters& CaloFutureCorrectionBase::getParams( const CaloFutureCorrection::Type type,
                                                                             const LHCb::CaloCellID id ) const {
  auto area = id.area();

  return m_params[area][type];
}

double CaloFutureCorrectionBase::getCorrection( const CaloFutureCorrection::Type type, const LHCb::CaloCellID id,
                                                double var, double def ) const {

  const auto& pars = getParams( type, id );
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    const auto& name = CaloFutureCorrection::typeName[type];
    debug() << "Correction type " << name << " to be applied on cluster (seed = " << id << ") is a '"
            << CaloFutureCorrection::funcName[pars.first] << "' function with params = " << pars.second << endmsg;
  }

  // compute correction
  if ( pars.first == CaloFutureCorrection::Unknown || pars.second.empty() ) return def;

  // list accessor - not correction :
  if ( pars.first == CaloFutureCorrection::ParamList || pars.first == CaloFutureCorrection::GlobalParamList ) {
    warning() << " Param accessor is a fake function - no correction to be applied - return default value" << endmsg;
    return def;
  }

  double cor = def;
  // polynomial correction
  const auto& temp = pars.second;

  // polynomial functions
  if ( pars.first == CaloFutureCorrection::Polynomial || pars.first == CaloFutureCorrection::InversPolynomial ||
       pars.first == CaloFutureCorrection::ExpPolynomial || pars.first == CaloFutureCorrection::ReciprocalPolynomial ) {
    double v = 1.;
    cor      = 0.;
    for ( auto i = temp.begin(); i != temp.end(); ++i ) {
      cor += ( *i ) * v;
      if ( pars.first == CaloFutureCorrection::ReciprocalPolynomial )
        v = ( var == 0 ) ? 0. : v / var;
      else
        v *= var;
#if defined( __clang__ )
      // Without this, clang optimiser does something with this loop that causes FPE...
      if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) ) verbose() << "cor = " << cor << endmsg;
#endif
    }
    if ( pars.first == CaloFutureCorrection::InversPolynomial ) cor = ( cor == 0 ) ? def : 1. / cor;
    if ( pars.first == CaloFutureCorrection::ExpPolynomial ) cor = ( cor == 0 ) ? def : myexp( cor );
  }

  // sigmoid function
  else if ( pars.first == CaloFutureCorrection::Sigmoid ) {
    if ( temp.size() == 4 ) {
      const auto& a = temp[0];
      const auto& b = temp[1];
      const auto& c = temp[2];
      const auto& d = temp[3];
      cor           = a + b * mytanh( c * ( var + d ) );
    } else {
      Warning( "The power sigmoid function must have 4 parameters" ).ignore();
    }
  }

  // Sshape function
  else if ( pars.first == CaloFutureCorrection::Sshape || pars.first == CaloFutureCorrection::SshapeMod ) {
    if ( temp.size() == 1 ) {
      const auto&      b     = temp[0];
      constexpr double delta = 0.5;
      if ( b > 0 ) {
        double arg = var / delta;
        if ( pars.first == CaloFutureCorrection::SshapeMod ) {
          arg *= mysinh( delta / b );
        } else if ( pars.first == CaloFutureCorrection::Sshape ) {
          arg *= mycosh( delta / b );
        }
        cor = b * mylog( arg + std::sqrt( arg * arg + 1.0 ) );
      }
    } else {
      Warning( "The Sshape function must have 1 parameter" ).ignore();
    }
  }

  // Shower profile function
  else if ( pars.first == CaloFutureCorrection::ShowerProfile ) {
    if ( temp.size() == 10 ) {
      if ( var > 0.5 ) {
        cor = temp[0] * myexp( -temp[1] * var );
        cor += temp[2] * myexp( -temp[3] * var );
        cor += temp[4] * myexp( -temp[5] * var );
      } else {
        cor = 2.;
        cor -= temp[6] * myexp( -temp[7] * var );
        cor -= temp[8] * myexp( -temp[9] * var );
      }
    } else {
      Warning( "The ShowerProfile function must have 10 parameters" ).ignore();
    }
  }

  // Sinusoidal function
  else if ( pars.first == CaloFutureCorrection::Sinusoidal ) {
    if ( temp.size() == 1 ) {
      const double& A = temp[0];
      cor             = A * mysin( 2 * M_PI * var );
    } else {
      Warning( "The Sinusoidal function must have 1 parameter" ).ignore();
    }
  }

  if ( counterStat->isVerbose() ) kounter( type, id.areaName() ) += cor;

  return cor;
}

void CaloFutureCorrectionBase::checkParams() {
  for ( int area = 0; area < 3; ++area ) {
    if ( static_cast<int>( m_params[area].size() ) != static_cast<int>( CaloFutureCorrection::nT ) ) {
      warning() << "Corrections vector size != " << CaloFutureCorrection::nT << endmsg;
    }

    for ( int itype = 0; itype < static_cast<int>( CaloFutureCorrection::nT ); ++itype ) {
      int  func = m_params[area][itype].first;
      bool ok   = true;
      if ( func != CaloFutureCorrection::Empty ) {
        if ( m_params[area][itype].second.size() == 0 ) {
          warning() << "Empty parameter vector of type " << CaloFutureCorrection::typeName[itype] << " of function "
                    << CaloFutureCorrection::funcName[func] << endmsg;
          ok = false;
        }
        if ( func >= CaloFutureCorrection::Unknown ) {
          warning() << " o Function for correction of type'" << CaloFutureCorrection::typeName[itype]
                    << "' is  not defined." << endmsg;
          ok = false;
        }
      }
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) && ok ) )
        debug() << " o Will apply correction '" << CaloFutureCorrection::funcName[itype] << "' as a '"
                << CaloFutureCorrection::funcName[func] << "' function of " << m_params[area][itype].second.size()
                << " parameters" << endmsg;
      if ( !ok ) {
        m_params[area][itype].second.clear();
        m_params[area][itype].first = CaloFutureCorrection::Empty;
      }
    }
  }
}

double CaloFutureCorrectionBase::getCorrectionDerivative( const CaloFutureCorrection::Type type,
                                                          const LHCb::CaloCellID id, double var, double def ) const {
  const auto& pars = getParams( type, id );

  if ( msgLevel( MSG::DEBUG ) ) {
    const auto name = CaloFutureCorrection::typeName[type];
    debug() << "Derivative for Correction type " << name << " to be calculated for cluster (seed = " << id << ") is a '"
            << CaloFutureCorrection::funcName[pars.first] << "' function with params = " << pars.second << endmsg;
  }

  // compute correction
  double cor = def;
  if ( pars.first == CaloFutureCorrection::Unknown || pars.second.empty() ) return cor;

  // polynomial correction
  const auto& temp = pars.second;

  // polynomial functions
  double ds = 0.;
  if ( pars.first == CaloFutureCorrection::Polynomial ) {
    ds         = 0.;
    double v   = 1.;
    int    cnt = 0;
    auto   i   = temp.begin();
    for ( ++i, cnt++; i != temp.end(); ++i, cnt++ ) {
      ds += ( *i ) * cnt * v;
      v *= var;
    }
  }

  else if ( pars.first == CaloFutureCorrection::InversPolynomial ) {
    double v = 1.;
    cor      = 0.;
    for ( auto i = temp.begin(); i != temp.end(); ++i ) {
      cor += ( *i ) * v;
      v *= var;
    }
    cor = ( cor == 0 ) ? def : 1. / cor;

    v        = 1.;
    ds       = 0.;
    int  cnt = 0;
    auto i   = temp.begin();
    for ( ++i, cnt++; i != temp.end(); ++i, cnt++ ) {
      ds += ( *i ) * cnt * v;
      v *= var;
    }
    ds *= -cor * cor;
  }

  else if ( pars.first == CaloFutureCorrection::ExpPolynomial ) {
    double v = 1.;
    cor      = 0.;
    for ( auto i = temp.begin(); i != temp.end(); ++i ) {
      cor += ( *i ) * v;
      v *= var;
    }
    cor = ( cor == 0 ) ? def : myexp( cor );

    ds       = 0.;
    v        = 1.;
    int  cnt = 0;
    auto i   = temp.begin();
    for ( ++i, cnt++; i != temp.end(); ++i, cnt++ ) {
      ds += ( *i ) * cnt * v;
      v *= var;
    }

    ds *= cor;
  }

  else if ( pars.first == CaloFutureCorrection::ReciprocalPolynomial ) {
    ds = 0.;
    if ( var != 0 ) {
      auto v   = 1. / ( var * var );
      int  cnt = 0;
      auto i   = temp.begin();
      for ( ++i, cnt++; i != temp.end(); ++i, cnt++ ) {
        ds -= ( *i ) * cnt * v;
        v /= var;
      }
    }
  }

  // sigmoid function
  else if ( pars.first == CaloFutureCorrection::Sigmoid ) {
    ds = 0.;
    if ( temp.size() == 4 ) {
      // double a = temp[0];
      const auto& b = temp[1];
      const auto& c = temp[2];
      const auto& d = temp[3];
      ds            = b * c * ( 1. - std::pow( mytanh( c * ( var + d ) ), 2 ) );
    } else {
      Warning( "The power sigmoid function must have 4 parameters" ).ignore();
    }
  }

  // Sshape function
  else if ( pars.first == CaloFutureCorrection::Sshape || pars.first == CaloFutureCorrection::SshapeMod ) {
    ds = 0.;
    if ( temp.size() == 1 ) {
      const auto& b     = temp[0];
      double      delta = 0.5;
      if ( b > 0 ) {
        double csh = 1.;
        if ( pars.first == CaloFutureCorrection::SshapeMod ) {
          csh = mysinh( delta / b );
        } else if ( pars.first == CaloFutureCorrection::Sshape ) {
          csh = mycosh( delta / b );
        }
        const auto arg = var / delta * csh;
        ds             = b / delta * csh / std::sqrt( arg * arg + 1. );
      }
    } else {
      Warning( "The Sshape function must have 1 parameter" ).ignore();
    }
  }

  // Shower profile function
  else if ( pars.first == CaloFutureCorrection::ShowerProfile ) {
    ds = 0.;
    if ( temp.size() == 10 ) {
      if ( var > 0.5 ) {
        ds = -temp[0] * temp[1] * myexp( -temp[1] * var );
        ds += -temp[2] * temp[3] * myexp( -temp[3] * var );
        ds += -temp[4] * temp[5] * myexp( -temp[5] * var );
      } else {
        ds = temp[6] * temp[7] * myexp( -temp[7] * var );
        ds += temp[8] * temp[9] * myexp( -temp[9] * var );
      }
    } else {
      Warning( "The ShowerProfile function must have 10 parameters" ).ignore();
    }
  }

  // Sinusoidal function
  else if ( pars.first == CaloFutureCorrection::Sinusoidal ) {
    ds = 0.;
    if ( temp.size() == 1 ) {
      const auto&      A     = temp[0];
      constexpr double twopi = 2. * M_PI;
      ds                     = A * twopi * mycos( twopi * var );
    } else {
      Warning( "The Sinusoidal function must have 1 parameter" ).ignore();
    }
  }

  return ds;
}
