/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureCorrectionBase.h"
#include "Event/CaloCluster.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Counters.h"
#include "ICaloFutureHypoTool.h"
#include "ICaloFutureShowerOverlapTool.h" // Interface

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloDataFunctor.h"

#include "boost/container/flat_map.hpp"
#include <functional>
#include <utility>

/** @class CaloFutureShowerOverlapTool CaloFutureShowerOverlapTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */

namespace {
  std::string correctionType( const std::string& name ) {
    const std::string uName( LHCb::CaloFutureAlgUtils::toUpper( name ) );
    if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos )
      return "SplitPhoton";
    return "Photon";
  }
} // namespace

namespace LHCb::Calo::Tools {

  class ShowerOverlap final : public extends<GaudiTool, Interfaces::IShowerOverlap> {

  public:
    using extends::extends;

    StatusCode initialize() override;

    void process( CaloCluster& c1, CaloCluster& c2, int niter = 10,
                  propagateInitialWeights = propagateInitialWeights{false} ) const override;

  private:
    void evaluate( CaloCluster& c, bool hypoCorrection = true ) const;

    Gaudi::Property<unsigned int>         m_minSize{this, "ClusterMinSize", 2};
    Gaudi::Property<std::string>          m_detLoc{this, "Detector", DeCalorimeterLocation::Ecal};
    Gaudi::Property<std::string>          m_pcond{this, "Profile", "Conditions/Reco/Calo/PhotonShowerProfile"};
    ToolHandle<Interfaces::IProcessHypos> m_stool{this, "CaloFutureSCorrection",
                                                  "CaloFutureSCorrection/" + correctionType( name() ) + "SCorrection"};
    ToolHandle<LHCb::Calo::Interfaces::IProcessHypos> m_ltool{
        this, "CaloFutureLCorrection", "CaloFutureLCorrection/" + correctionType( name() ) + "LCorrection"};
    ToolHandle<CaloFutureCorrectionBase>   m_shape{this, "CaloFutureCorrectionBase",
                                                 "CaloFutureCorrectionBase/ShowerProfile"};
    const DeCalorimeter*                   m_det = nullptr;
    mutable Gaudi::Accumulators::Counter<> m_skippedSize{this, "Overlap correction skipped due to cluster size"};
    mutable Gaudi::Accumulators::Counter<> m_skippedEnergy{this, "Overlap correction skipped due to cluster energy"};
    mutable Gaudi::Accumulators::Counter<> m_positionFailed{this, "Cluster position failed"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_lcorr_error{this, "LCorrection could not be evaluated"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_scorr_error{this, "SCorrection could not be evaluated"};
  };

  namespace {

    using WeightMap = boost::container::flat_map<CaloCellID, double>;

    double showerFraction( const CaloFutureCorrectionBase& shape, const double d3d, const unsigned int area ) {
      CaloCellID cellID( CaloCellCode::CaloIndex::EcalCalo, area, 0, 0 ); // fake cell
      return std::clamp( shape.getCorrection( CaloFutureCorrection::profile, cellID, d3d, 0. ), 0., 1. );
    }

    double fraction( const CaloFutureCorrectionBase& shape, const DeCalorimeter& det, const CaloCluster& cluster,
                     const CaloDigit& digit, int area ) {

      const CaloCellID cellID = digit.cellID();

      double size = det.cellSize( cellID );
      double xd   = det.cellX( cellID );
      double yd   = det.cellY( cellID );
      double xc   = cluster.position().parameters()( CaloPosition::Index::X );
      double yc   = cluster.position().parameters()( CaloPosition::Index::Y );
      double zc   = cluster.position().z();
      double zd   = ( xc * xc + yc * yc + zc * zc - xc * xd - yc * yd ) / zc;
      double d3d =
          std::sqrt( ( xd - xc ) * ( xd - xc ) + ( yd - yc ) * ( yd - yc ) + ( zd - zc ) * ( zd - zc ) ) / size;
      double f  = showerFraction( shape, d3d, area );
      double ed = digit.e();
      double ec = f * cluster.position().parameters()( CaloPosition::Index::E );
      return ( ed > ec ) ? ( ed - ec ) / ed : 0.;
    }

    template <typename Map, typename Predicate>
    void erase_if( Map& map, Predicate&& predicate ) {
      auto i = map.begin();
      while ( i != map.end() ) {
        if ( std::invoke( predicate, std::as_const( *i ) ) )
          i = map.erase( i );
        else
          ++i;
      }
    }

    template <typename Map>
    Map makeWeights( const CaloCluster& cl1, const CaloCluster& cl2 ) {
      Map weights;
      weights.reserve( cl1.entries().size() + cl2.entries().size() );
      for ( const auto& i1 : cl1.entries() ) {
        [[maybe_unused]] auto [it, inserted] = weights.try_emplace( i1.digit()->cellID(), i1.fraction() );
        assert( inserted );
      }
      for ( const auto& i2 : cl2.entries() ) {
        auto [it, inserted] = weights.try_emplace( i2.digit()->cellID(), i2.fraction() );
        if ( !inserted ) it->second += i2.fraction();
      }
      // check
      erase_if( weights, []( const auto& p ) { return p.second == 1.; } );
      return weights;
    }

    template <typename GetWeight, typename GetFraction, typename Evaluate>
    void subtract( CaloCluster& cl1, CaloCluster& cl2, const GetWeight& getWeight, const GetFraction& getFraction,
                   const Evaluate& evaluate ) {

      auto skip = []( auto status ) {
        return ( CaloDigitStatus::UseForEnergy & status ) == 0 && ( CaloDigitStatus::UseForPosition & status ) == 0;
      };

      // cluster1  -> cluster2 spillover
      for ( auto& i2 : cl2.entries() ) {
        if ( skip( i2.status() ) ) continue;
        i2.setFraction( getFraction( cl1, *i2.digit(), 1 ) * getWeight( *i2.digit() ) );
      }

      // re-evaluate cluster2 accordingly
      evaluate( cl2 );
      if ( cl2.e() < 0 ) return; // skip negative energy "clusters"

      // cluster2  -> cluster1 spillover
      for ( auto& i1 : cl1.entries() ) {
        const CaloDigit* dig1 = i1.digit();
        if ( skip( i1.status() ) ) continue;
        double initialWeight = getWeight( *dig1 );
        i1.setFraction( getFraction( cl2, *dig1, 2 ) * initialWeight );
        constexpr double eps = 1.e-4;
        // normalize the sum of partial weights in case of  shared cells
        for ( auto& i2 : cl2.entries() ) {
          const CaloDigit* dig2 = i2.digit();
          if ( !( dig2->cellID() == dig1->cellID() ) || skip( i2.status() ) ) continue;
          double f1  = i1.fraction();
          double f2  = i2.fraction();
          double sum = f1 + f2;
          if ( std::abs( sum - initialWeight ) > eps ) {
            if ( sum < initialWeight && f2 == 0 ) {
              i2.setFraction( initialWeight - f1 );
            } else if ( sum < initialWeight && f1 == 0 ) {
              i1.setFraction( initialWeight - f2 );
            } else {
              i1.setFraction( initialWeight * f1 / ( f1 + f2 ) );
              i2.setFraction( initialWeight * f2 / ( f1 + f2 ) );
            }
          }
        }
      }

      // reevaluate  cluster1 & 2 accordingly
      evaluate( cl1 );
      evaluate( cl2 );
    }
  } // namespace

  //=============================================================================
  // Initializer
  //=============================================================================

  StatusCode ShowerOverlap::initialize() {
    StatusCode sc = GaudiTool::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc;         // error printed already by GaudiAlgorithm
    m_det = getDet<DeCalorimeter>( m_detLoc );
    m_shape.retrieve();
    return m_shape->setConditionParams( m_pcond, true );
  }

  //=============================================================================

  void ShowerOverlap::process( CaloCluster& cl1, CaloCluster& cl2, const int niter,
                               propagateInitialWeights propagateInitialWeights ) const {

    if ( cl1.entries().size() < m_minSize || cl2.entries().size() < m_minSize ) {
      ++m_skippedSize;
      return; // skip small clusters
    }

    // 0 - evaluate parameters (applying photon hypo corrections for the position)
    evaluate( cl1 );
    evaluate( cl2 );

    if ( cl1.e() <= 0. || cl2.e() <= 0 ) {
      ++m_skippedEnergy;
      return;
    }

    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << " ======== Shower Overlap =======" << endmsg;
      verbose() << " CL1/CL2 : " << cl1.e() << " " << cl2.e() << " " << cl1.e() + cl2.e() << endmsg;
      verbose() << " seed    : " << cl1.seed() << " " << cl2.seed() << endmsg;
      verbose() << " area    : " << cl1.seed().area() << " " << cl2.seed().area() << endmsg;
      verbose() << " params  : " << cl1.position().parameters() << " / " << cl2.position().parameters() << endmsg;
    }

    const auto evaluate_ = [this]( CaloCluster& cluster, bool c = true ) { return this->evaluate( cluster, c ); };
    const auto weight_   = [weights = ( propagateInitialWeights ? makeWeights<WeightMap>( cl1, cl2 ) : WeightMap{} )](
                             const CaloDigit& d ) {
      if ( weights.empty() ) return 1.;
      auto it = weights.find( d.cellID() );
      return it != weights.end() ? it->second : 1.;
    };
    const auto fraction_ = [shape = std::cref( *m_shape ), det = std::cref( *m_det ), a1 = cl1.seed().area(),
                            a2 = cl2.seed().area()]( const CaloCluster& c, const CaloDigit& d, int flag ) {
      return fraction( shape, det, c, d, flag == 1 ? a1 : a2 );
    };
    // 1 - determine the energy fractions of each entry
    for ( int iter = 0; iter < niter; ++iter ) {

      subtract( cl1, cl2, weight_, fraction_, evaluate_ );

      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " ------ iter = " << iter << endmsg;
        verbose() << " >> CL1/CL2 : " << cl1.e() << " " << cl2.e() << "  " << cl1.e() + cl2.e() << endmsg;
        verbose() << " >> params  : " << cl1.position().parameters() << " / " << cl2.position().parameters() << endmsg;
        CaloMomentum momentum;
        momentum.addCaloPosition( &cl1 );
        momentum.addCaloPosition( &cl2 );
        verbose() << " >> Mass : " << momentum.mass() << endmsg;
      }
    }
    // 3 - reset cluster-like parameters
    evaluate( cl1, false );
    evaluate( cl2, false );
  }

  void ShowerOverlap::evaluate( CaloCluster& cluster, bool hypoCorrection ) const {

    // 0 - reset z-position of cluster
    LHCb::ClusterFunctors::ZPosition zPosition( m_det );
    cluster.position().setZ( zPosition( &cluster ) );

    // 1 - 3x3 energy and energy-weighted barycenter
    double     E, X, Y;
    StatusCode sc =
        LHCb::ClusterFunctors::calculateEXY( cluster.entries().begin(), cluster.entries().end(), m_det, E, X, Y );
    if ( sc.isSuccess() ) {
      cluster.position().parameters()( CaloPosition::Index::E ) = E;
      cluster.position().parameters()( CaloPosition::Index::X ) = X;
      cluster.position().parameters()( CaloPosition::Index::Y ) = Y;
    } else {
      ++m_positionFailed;
    }

    if ( cluster.e() < 0 ) return; // skip correction for negative energy "clusters"

    if ( !hypoCorrection ) return; // do not apply 'photon' hypo correction

    // 2 - apply 'photon hypothesis' corrections

    // create a fake CaloHypo
    CaloHypo hypo{};
    hypo.setHypothesis( CaloHypo::Hypothesis::Photon );
    hypo.addToClusters( &cluster );
    hypo.setPosition( std::make_unique<CaloPosition>( cluster.position() ) );

    // Apply transversal corrections
    sc = m_stool->process( hypo );
    if ( sc.isSuccess() ) {
      cluster.position().parameters()( CaloPosition::Index::X ) =
          hypo.position()->parameters()( CaloPosition::Index::X );
      cluster.position().parameters()( CaloPosition::Index::Y ) =
          hypo.position()->parameters()( CaloPosition::Index::Y );
    } else {
      ++m_scorr_error;
    }

    // Apply longitudinal correction
    sc = m_ltool->process( hypo );
    if ( sc.isSuccess() ) {
      cluster.position().setZ( hypo.position()->z() );
    } else {
      ++m_lcorr_error;
    }
  }
} // namespace LHCb::Calo::Tools

// Declaration of the Tool Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::Tools::ShowerOverlap, "CaloFutureShowerOverlapTool" )
