/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
// from LHCb
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/LineTypes.h"
// local
#include "CaloFutureHypo2CaloFuture.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureHypo2CaloFuture
//
// 2008-09-11 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureHypo2CaloFuture )

//=============================================================================
StatusCode CaloFutureHypo2CaloFuture::initialize() {
  StatusCode sc = CaloFuture2CaloFuture::initialize();
  return sc;
}

double CaloFutureHypo2CaloFuture::energy( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo ) const {
  const auto& output = cellIDs( fromCluster, toCalo, fetch_digits( toCalo ) );
  return std::accumulate( output.begin(), output.end(), 0., []( double e, const auto* d ) { return e + d->e(); } );
}
double CaloFutureHypo2CaloFuture::energy( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const {
  const auto& output = cellIDs( fromHypo, toCalo, fetch_digits( toCalo ) );
  return std::accumulate( output.begin(), output.end(), 0., []( double e, const auto* d ) { return e + d->e(); } );
}
int CaloFutureHypo2CaloFuture::multiplicity( const LHCb::CaloCluster& fromCluster,
                                             CaloCellCode::CaloIndex  toCalo ) const {
  return cellIDs( fromCluster, toCalo, fetch_digits( toCalo ) ).size();
}
int CaloFutureHypo2CaloFuture::multiplicity( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const {
  return cellIDs( fromHypo, toCalo, fetch_digits( toCalo ) ).size();
}

//=============================================================================
std::vector<const LHCb::CaloDigit*> CaloFutureHypo2CaloFuture::cellIDs( const LHCb::CaloHypo&   fromHypo,
                                                                        CaloCellCode::CaloIndex toCalo,
                                                                        const LHCb::CaloDigits& digits ) const {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Matching CaloHypo to " << toCalo << " hypo energy = " << fromHypo.e() << endmsg;

  // get the cluster
  const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( &fromHypo );

  if ( !cluster ) {
    Error( "No valid cluster!" ).ignore();
    return {};
  }

  LHCb::CaloCellID seedID   = cluster->seed();
  auto             fromCalo = seedID.calo();
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Cluster seed " << seedID << " " << m_det[fromCalo]->cellCenter( seedID ) << endmsg;

  if ( m_whole ) { return CaloFuture2CaloFuture::cellIDs( *cluster, toCalo, digits ); }

  auto lineID = LHCb::CaloCellID();
  if ( m_line && fromHypo.position() ) {
    const Gaudi::XYZPoint  ref( fromHypo.position()->x(), fromHypo.position()->y(), fromHypo.position()->z() );
    const Gaudi::XYZVector vec = ( ref - Gaudi::XYZPoint( 0, 0, 0 ) );
    Gaudi::Math::XYZLine   line( ref, vec );
    double                 mu;
    auto                   point = Gaudi::XYZPoint();
    Gaudi::Math::intersection( line, m_plane[toCalo], point, mu );
    lineID = m_det[toCalo]->Cell( point );
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Matching cell " << lineID << endmsg;
  }
  return cellIDs( *cluster, toCalo, digits, lineID );
}

std::vector<const LHCb::CaloDigit*> CaloFutureHypo2CaloFuture::cellIDs( const LHCb::CaloCluster& fromCluster,
                                                                        CaloCellCode::CaloIndex  toCalo,
                                                                        const LHCb::CaloDigits&  digits,
                                                                        LHCb::CaloCellID         lineID ) const {
  std::vector<const LHCb::CaloDigit*> output;
  LHCb::CaloCellID                    seedID   = fromCluster.seed();
  auto                                fromCalo = seedID.calo();

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "-----  cluster energy " << fromCluster.e() << " " << seedID << endmsg;
  CellNeighbour neighbour;
  neighbour.setDet( m_det[fromCalo] );

  // matching cluster
  for ( const LHCb::CaloClusterEntry& entry : fromCluster.entries() ) {
    LHCb::CaloCellID cellID = entry.digit()->cellID();
    if ( !( m_seed && ( LHCb::CaloDigitStatus::SeedCell & entry.status() ) != 0 ) &&
         !( m_seed && m_neighb && neighbour( seedID, cellID ) != 0. ) && !( ( m_status & entry.status() ) != 0 ) &&
         !( m_whole ) )
      continue;
    output = cellIDs_( entry.digit()->cellID(), toCalo, digits, std::move( output ) );
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << toCalo << ":  digit is selected in front of the cluster : " << cellID << "/" << seedID << " "
              << output.size() << endmsg;
  }
  // photon line
  if ( m_line ) {
    auto point = Gaudi::XYZPoint();
    if ( lineID == LHCb::CaloCellID() ) {
      const Gaudi::XYZPoint  ref( fromCluster.position().x(), fromCluster.position().y(), fromCluster.position().z() );
      const Gaudi::XYZVector vec = ( ref - Gaudi::XYZPoint( 0, 0, 0 ) );
      Gaudi::Math::XYZLine   line( ref, vec );
      double                 mu;
      Gaudi::Math::intersection( line, m_plane[toCalo], point, mu );
      lineID = m_det[toCalo]->Cell( point );
    }
    if ( !( lineID == LHCb::CaloCellID() ) ) {
      assert( lineID.calo() == toCalo );
      output = addCell( lineID, digits, std::move( output ) );
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << toCalo << " : digit is selected in the photon line : " << lineID << "/" << seedID << " "
                << output.size() << endmsg;
      if ( m_neighb ) {
        for ( const auto& n : m_det[toCalo]->neighborCells( lineID ) ) {
          assert( n.calo() == toCalo );
          double                halfCell   = m_det[toCalo]->cellSize( n ) * 0.5;
          const Gaudi::XYZPoint cellCenter = m_det[toCalo]->cellCenter( n );
          if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
            debug() << n << " Point : (" << point.X() << "," << point.Y() << " Cell :  ( " << cellCenter.X() << ","
                    << cellCenter.Y() << " size/2  : " << halfCell << " Tolerance : " << m_x << "/" << m_y << endmsg;
          if ( fabs( point.X() - cellCenter.X() ) < ( halfCell + m_x ) &&
               fabs( point.Y() - cellCenter.Y() ) < ( halfCell + m_y ) ) {
            output = addCell( n, digits, std::move( output ) );
            if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
              debug() << toCalo << " : digit is selected in the photon line neighborhood : " << n << "/" << seedID
                      << " " << output.size() << endmsg;
          }
        }
      }
    }
  }
  return output;
}
