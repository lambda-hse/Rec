/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREHYPO2CALOFUTURE_H
#define CALOFUTUREHYPO2CALOFUTURE_H 1

// Include files
// from Gaudi
#include "CaloFuture2CaloFuture.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h" // Interface
#include "CaloFutureUtils/CellNeighbour.h"
#include "GaudiAlg/GaudiTool.h"

/** @class CaloFutureHypo2CaloFuture CaloFutureHypo2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-09-11
 */
class CaloFutureHypo2CaloFuture final : public extends<CaloFuture2CaloFuture, LHCb::Calo::Interfaces::IHypo2Calo> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;

  // energy
  double energy( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo ) const override;
  double energy( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const override;

  // multiplicity
  int multiplicity( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo ) const override;
  int multiplicity( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const override;

private:
  const LHCb::CaloDigits& fetch_digits( CaloCellCode::CaloIndex toCalo ) const { return *m_handles[toCalo].get(); }
  // cellIDs
  using CaloFuture2CaloFuture::cellIDs;
  std::vector<const LHCb::CaloDigit*> cellIDs( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo,
                                               const LHCb::CaloDigits& digits, LHCb::CaloCellID lineID ) const;
  std::vector<const LHCb::CaloDigit*> cellIDs( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo,
                                               const LHCb::CaloDigits& digits ) const;

  Gaudi::Property<bool>                       m_seed{this, "Seed", true};
  Gaudi::Property<bool>                       m_neighb{this, "AddNeighbors", true};
  Gaudi::Property<bool>                       m_line{this, "PhotonLine", true};
  Gaudi::Property<bool>                       m_whole{this, "WholeCluster", false};
  Gaudi::Property<int>                        m_status{this, "StatusMask", 0x0};
  Gaudi::Property<float>                      m_x{this, "xTolerance", 5. * Gaudi::Units::mm};
  Gaudi::Property<float>                      m_y{this, "yTolerance", 5. * Gaudi::Units::mm};
  Map<DataObjectReadHandle<LHCb::CaloDigits>> m_handles{
      std::forward_as_tuple( this, "EcalDigitsLocation", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Ecal" ) ),
      std::forward_as_tuple( this, "HcalDigitsLocation", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Hcal" ) )};
};
#endif // CALOFUTUREHYPO2CALOFUTURE_H
