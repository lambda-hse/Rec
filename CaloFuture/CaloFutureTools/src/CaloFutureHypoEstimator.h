/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREHYPOESTIMATOR_H
#define CALOFUTUREHYPOESTIMATOR_H 1

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h" // Interface
#include "CaloFutureInterfaces/ICaloFutureRelationsGetter.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureInterfaces/IFutureGammaPi0SeparationTool.h"
#include "CaloFutureInterfaces/IFutureNeutralIDTool.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "Event/CaloDataFunctor.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/IRelationWeighted2D.h"
#include "Relations/Relation2D.h"

/** @class CaloFutureHypoEstimator CaloFutureHypoEstimator.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-08-18
 */

namespace LHCb::Calo {

  class HypoEstimator : public extends<GaudiTool, Interfaces::IHypoEstimator, IIncidentListener> {
  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;
    StatusCode finalize() override;

    std::optional<double> data( const LHCb::CaloCluster& cluster, Enum::DataType type ) const override;
    std::optional<double> data( const LHCb::CaloHypo& hypo, Enum::DataType type ) const override;

    void handle( const Incident& ) override {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "IIncident Svc reset" << endmsg;
      clean();
    }

    Interfaces::IHypo2Calo* hypo2Calo() override { return m_toCaloFuture.get(); }

    const LHCb::Track* toTrack( Enum::MatchType match ) const override {
      auto it = m_track.find( match );
      return it != m_track.end() ? it->second : nullptr;
    }

    StatusCode _setProperty( const std::string& p, const std::string& v ) override { return setProperty( p, v ); };
    bool       status() const override { return m_status; }

  private:
    bool estimator( const LHCb::CaloCluster& cluster, const LHCb::CaloHypo* fromHypo = nullptr ) const;
    bool estimator( const LHCb::CaloHypo& hypo ) const;
    void clean() const;

    mutable std::map<Enum::DataType, double>              m_data;
    mutable std::map<Enum::MatchType, const LHCb::Track*> m_track;
    mutable const LHCb::CaloHypo*                         m_hypo    = nullptr;
    mutable const LHCb::CaloCluster*                      m_cluster = nullptr;

    mutable std::map<std::string, const LHCb::CaloFuture2Track::IHypoEvalTable*> m_idTable;
    mutable bool                                                                 m_status = true;

    Gaudi::Property<bool>                               m_extrapol{this, "Extrapolation", true};
    Gaudi::Property<bool>                               m_seed{this, "AddSeed", false};
    Gaudi::Property<bool>                               m_neig{this, "AddNeighbors", false};
    Gaudi::Property<std::map<std::string, std::string>> m_pidLoc{
        this,
        "NeutralIDLocations",
        {{"Photon", LHCb::CaloFutureIdLocation::PhotonID},
         {"Pi0Merged", LHCb::CaloFutureIdLocation::MergedID},
         {"PhotonFromMergedPi0", LHCb::CaloFutureIdLocation::PhotonFromMergedID}}};
    Gaudi::Property<std::string> m_cmLoc{this, "ClusterMatchLocation",
                                         LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" )};
    Gaudi::Property<std::string> m_emLoc{this, "ElectronMatchLocation",
                                         LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ElectronMatch" )};
    Gaudi::Property<std::string> m_bmLoc{this, "BremMatchLocation",
                                         LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "BremMatch" )};
    Gaudi::Property<bool>        m_skipC{this, "SkipChargedID", false};
    Gaudi::Property<bool>        m_skipN{this, "SkipNeutralID", false};
    Gaudi::Property<bool>        m_skipCl{this, "SkipClusterMatch", false};

    PublicToolHandle<IFutureCounterLevel>               counterStat{this, "CounterLevel", "FutureCounterLevel"};
    ToolHandle<Interfaces::IHypo2Calo>                  m_toCaloFuture{this, "Hypo2Calo", "CaloFutureHypo2CaloFuture"};
    mutable ToolHandle<Interfaces::IElectron>           m_electron{this, "Electron", "CaloFutureElectron"};
    mutable ToolHandle<Interfaces::IGammaPi0Separation> m_GammaPi0{this, "Pi0Separation",
                                                                   "FutureGammaPi0SeparationTool"};
    mutable PublicToolHandle<Interfaces::IGammaPi0Separation> m_GammaPi0XGB{this, "PiOSeparation",
                                                                            "FutureGammaPi0XGBoostTool"};
    ToolHandle<Interfaces::INeutralID>                        m_neutralID{this, "NeutralID", "FutureNeutralIDTool"};
    ToolHandle<ICaloFutureRelationsGetter> m_tables{this, "RelationGetter", "CaloFutureRelationsGetter"};

    DeCalorimeter* m_ecal = nullptr;
  };
} // namespace LHCb::Calo
#endif // CALOFUTUREHYPOESTIMATOR_H
