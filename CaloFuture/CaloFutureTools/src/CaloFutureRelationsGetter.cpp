/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"

// local
#include "CaloFutureRelationsGetter.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureRelationsGetter
//
// 2013-10-04 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureRelationsGetter )

//=============================================================================

StatusCode CaloFutureRelationsGetter::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Initialize CaloFutureRelationsGetter tool " << endmsg;
  // subscribe to the incidents
  IIncidentSvc* inc = incSvc();
  if ( inc ) inc->addListener( this, IncidentType::BeginEvent );
  return sc;
}

StatusCode CaloFutureRelationsGetter::finalize() {
  IIncidentSvc* inc = incSvc();
  if ( inc ) { inc->removeListener( this ); }
  return GaudiTool::finalize();
}

const LHCb::CaloFuture2Track::ITrHypoTable2D*
CaloFutureRelationsGetter::getTrHypoTable2D( std::string location ) const {
  auto self = const_cast<CaloFutureRelationsGetter*>( this );
  auto it   = m_hypoTr.find( location );
  if ( it == m_hypoTr.end() )
    self->m_hypoTr[location] = getIfExists<LHCb::CaloFuture2Track::IHypoTrTable2D>( location );

  self->m_trHypo.i_clear().ignore();
  for ( const auto& l : self->m_hypoTr[location]->relations() ) {
    self->m_trHypo.i_push( l.to(), l.from(), l.weight() );
  }
  self->m_trHypo.i_sort();
  return &m_trHypo;
}

void CaloFutureRelationsGetter::clean() { m_hypoTr.clear(); }
