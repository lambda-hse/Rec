/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "FutureNeutralProtoParticleAddNeutralID.h"
#include "CaloFutureUtils/CaloMomentum.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FutureNeutralProtoParticleAddNeutralID
//
// 2014-05-27 : Olivier Deschamps
//-----------------------------------------------------------------------------

namespace {

  enum class func_type { none, abs, area };

  bool addInput( double& data, const LHCb::ProtoParticle* proto, const LHCb::ProtoParticle::additionalInfo flag,
                 const func_type func = func_type::none ) {
    auto v = proto->info( flag, -1e+06 );
    if ( func == func_type::abs ) {
      v = fabs( v );
    } else if ( func == func_type::area ) {
      v = double( ( int( v ) >> 12 ) & 0x3 );
    }
    data = v;
    return proto->hasInfo( flag );
  }
} // namespace

//=============================================================================
// Main execution
//=============================================================================
StatusCode FutureNeutralProtoParticleAddNeutralID::execute() {
  // locate input data
  for ( auto* proto : *get<LHCb::ProtoParticles>( evtSvc(), m_input ) ) {

    const auto& hypos = proto->calo();
    if ( hypos.empty() ) { continue; }

    const auto hypo = hypos.front();
    const auto pt   = LHCb::CaloMomentum( hypo ).pt();

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "---------- " << hypo->hypothesis() << " ---------- " << endmsg;

    //======== re-build neutralID
    if ( m_isNotE.value() || m_isNotH.value() ) {
      LHCb::Calo::Interfaces::INeutralID::Observables v;
      bool                                            nOk = true;
      nOk &= addInput( v.clmatch, proto, LHCb::ProtoParticle::additionalInfo::CaloTrMatch );
      nOk &= addInput( v.prse, proto, LHCb::ProtoParticle::additionalInfo::CaloDepositID, func_type::abs );
      nOk &= addInput( v.e19, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralE19 );
      nOk &= addInput( v.hclecl, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal );
      nOk &= addInput( v.prse19, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE19 );
      nOk &= addInput( v.prse49, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE49 );
      nOk &= addInput( v.sprd, proto, LHCb::ProtoParticle::additionalInfo::ShowerShape );
      nOk &= addInput( v.prse4mx, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE4max );
      nOk &= addInput( v.prsm, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralPrsM );
      nOk &= addInput( v.spdm, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralSpd );
      if ( m_isNotE.value() && nOk ) {
        const auto temp = proto->info( LHCb::ProtoParticle::additionalInfo::IsNotE, -1. );
        proto->eraseInfo( LHCb::ProtoParticle::additionalInfo::IsNotE );
        const auto val = ( pt > m_isNotE_Pt.value() ) ? m_neutralID->isNotE( v ) : -1.;
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::IsNotE, val );
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << "UPDATING IsNotE : " << temp << " ---> "
                  << proto->info( LHCb::ProtoParticle::additionalInfo::IsNotE, -1. ) << " ("
                  << proto->hasInfo( LHCb::ProtoParticle::additionalInfo::IsNotE ) << ")" << endmsg;
      }
      if ( m_isNotH.value() && nOk ) {
        const auto temp = proto->info( LHCb::ProtoParticle::additionalInfo::IsNotH, -1. );
        proto->eraseInfo( LHCb::ProtoParticle::additionalInfo::IsNotH );
        const auto val = ( pt > m_isNotH_Pt ) ? m_neutralID->isNotH( v ) : -1.;
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::IsNotH, val );
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << "UPDATING IsNotH : " << temp << " ---> "
                  << proto->info( LHCb::ProtoParticle::additionalInfo::IsNotH, -1. ) << " ("
                  << proto->hasInfo( LHCb::ProtoParticle::additionalInfo::IsNotH ) << ")" << endmsg;
      }
    }

    // re-build gamma/pi0 separation
    if ( m_isPhoton.value() && hypo->hypothesis() != LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 ) {
      double pV[15] = {};
      bool   pOk    = true;
      pOk &= addInput( pV[0], proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralID, func_type::area );
      pOk &= addInput( pV[1], proto, LHCb::ProtoParticle::additionalInfo::ShowerShape );
      pOk &= addInput( pV[2], proto, LHCb::ProtoParticle::additionalInfo::CaloShapeFr2r4 );
      pOk &= addInput( pV[3], proto, LHCb::ProtoParticle::additionalInfo::CaloShapeAsym );
      pOk &= addInput( pV[4], proto, LHCb::ProtoParticle::additionalInfo::CaloShapeKappa );
      pOk &= addInput( pV[5], proto, LHCb::ProtoParticle::additionalInfo::CaloShapeE1 );
      pOk &= addInput( pV[6], proto, LHCb::ProtoParticle::additionalInfo::CaloShapeE2 );
      pOk &= addInput( pV[7], proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeFr2 );
      pOk &= addInput( pV[8], proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeAsym );
      pOk &= addInput( pV[9], proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeEmax );
      pOk &= addInput( pV[10], proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeE2 );
      pOk &= addInput( pV[11], proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM );
      pOk &= addInput( pV[12], proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM15 );
      pOk &= addInput( pV[13], proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM30 );
      pOk &= addInput( pV[14], proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM45 );

      if ( m_isPhoton.value() && pOk ) {
        const auto temp = proto->info( LHCb::ProtoParticle::additionalInfo::IsPhoton, -1. );
        proto->eraseInfo( LHCb::ProtoParticle::additionalInfo::IsPhoton );
        const auto val = ( pt > m_isPhoton_Pt.value() ) ? m_gammaPi0->isPhoton( pV ).value_or( -1. ) : -1.;
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::IsPhoton, val );
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << "UPDATING IsPhoton : " << temp << " ---> "
                  << proto->info( LHCb::ProtoParticle::additionalInfo::IsPhoton, -1. ) << " ("
                  << proto->hasInfo( LHCb::ProtoParticle::additionalInfo::IsPhoton ) << ")" << endmsg;
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FutureNeutralProtoParticleAddNeutralID )

//=============================================================================
