###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Package: RecConf
###############################################################################
gaudi_subdir(RecConf)

gaudi_depends_on_subdirs(Calo/CaloMoniDst
                         Calo/CaloPIDs
                         Calo/CaloReco
			 CaloFuture/CaloFutureMoniDst
                         CaloFuture/CaloFuturePIDs
                         CaloFuture/CaloFutureReco
                         Kernel/LHCbKernel
                         Muon/MuonID
                         Muon/MuonPIDChecker
                         Muon/MuonTools
                         Muon/MuonTrackAlign
                         Muon/MuonTrackMonitor
                         Muon/MuonTrackRec
                         Rec/GlobalReco
                         Rec/LumiAlgs
		         Rec/RecAlgs
                         Rich/RichFutureRecSys
                         Rich/RichFutureRecMonitors
                         Rich/RichFutureRecCheckers
                         Tf/TrackSys
                         Tf/TsaAlgorithms
                         Tr/PatChecker
                         Tr/PatFitParams
                         Tr/PatPV
                         Tr/TrackAssociators
                         Tr/TrackCheckers
                         Tr/TrackExtrapolators
                         Tr/TrackFitEvent
                         Tr/TrackFitter
                         Tr/TrackIdealPR
                         Tr/TrackMCTools
                         Tr/TrackMonitors
                         Tr/TrackProjectors
                         Tr/TrackTools
                         Tr/TrackUtils)

gaudi_install_python_modules()
