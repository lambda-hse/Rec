/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <sstream>
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// Track Interfaces
// ============================================================================
#include "TrackInterfaces/ITrackSelector.h"
#include "TrackKernel/TrackVertexUtils.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Constants.h"
#include "LoKi/Track.h"
// ============================================================================
/** @file
 *  Implementation file for classes from the namespace LoKi::Tracks
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-08-08
 *
 */
// ============================================================================

// ============================================================================
// constructor from the flag
// ============================================================================
LoKi::Track::CheckFlag::CheckFlag( LHCb::Track::Flags flag )
    : LoKi::AuxFunBase( std::make_tuple( LoKi::StrKeep( "LHCb::Track::" + LHCb::Track::FlagsToString( flag ) ) ) )
    , m_flag( flag ) {}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Track::CheckFlag::fillStream( std::ostream& s ) const {
  switch ( m_flag ) {
  case LHCb::Track::Flags::Backward:
    return s << "TrBACKWARD"; // RETURN
  case LHCb::Track::Flags::Invalid:
    return s << "TrINVALID"; // RETURN
  case LHCb::Track::Flags::Clone:
    return s << "TrCLONE"; // RETURN
  case LHCb::Track::Flags::Used:
    return s << "TrUSED"; // RETURN
  case LHCb::Track::Flags::IPSelected:
    return s << "TrIPSELECTED"; // RETURN
  case LHCb::Track::Flags::PIDSelected:
    return s << "TrPIDSELECTED"; // RETURN
  case LHCb::Track::Flags::Selected:
    return s << "TrSELECTED"; // RETURN
  case LHCb::Track::Flags::L0Candidate:
    return s << "TrL0CANDIDATE"; // RETURN
  default:
    break; // BREAK
  }
  //
  return s << "TrISFLAG(" << (int)m_flag << ")";
}
// ============================================================================
// mandatory: the only one essential method
// ============================================================================
LoKi::Track::CheckFlag::result_type LoKi::Track::CheckFlag::operator()( LoKi::Track::CheckFlag::argument t ) const {
  //
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return 'false'" );
    return false;
  }
  //
  return t->checkFlag( m_flag );
}
// ============================================================================
// constructor form the tool
// ============================================================================
LoKi::Track::Selector::Selector( const ITrackSelector* tool ) : m_tool( const_cast<ITrackSelector*>( tool ) ) {
  Assert( m_tool.validPointer(), "ITrackSelector* point to NULL" );
}
// ============================================================================
// constructor form the tool
// ============================================================================
LoKi::Track::Selector::Selector( const LoKi::Interface<ITrackSelector>& tool ) : m_tool( tool ) {
  Assert( m_tool.validPointer(), "ITrackSelector* point to NULL" );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::Selector::result_type LoKi::Track::Selector::operator()( LoKi::Track::Selector::argument t ) const {
  Assert( m_tool.validPointer(), "ITrackSelector* points to NULL" );
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return 'false'" );
    return false;
  }
  return m_tool->accept( *t );
}
// ============================================================================
// set new selector tool
// ============================================================================
void LoKi::Track::Selector::setSelector( const ITrackSelector* selector ) const {
  m_tool = const_cast<ITrackSelector*>( selector );
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Track::Selector::fillStream( std::ostream& s ) const { return s << "TrSELECTOR"; }

// ============================================================================
/*  constructor from "info"
 *  @param key info index/mark/key
 */
// ============================================================================
// constructor from the key
// ============================================================================
LoKi::Track::HasInfo::HasInfo( const int key )
    : LoKi::AuxFunBase( std::tie( key ) ), LoKi::ExtraInfo::CheckInfo<const LHCb::Track*>( key ) {}
// ============================================================================
// the specific printout
// ============================================================================
std::ostream& LoKi::Track::HasInfo::fillStream( std::ostream& s ) const { return s << "TrHASINFO(" << index() << ")"; }
// ============================================================================
/* constructor from "info"
 *  @param key info index/mark/key
 *  @param def default value for missing key/invalid object
 */
// ============================================================================
LoKi::Track::Info::Info( const int key, const double def )
    : LoKi::AuxFunBase( std::tie( key, def ) ), LoKi::ExtraInfo::GetInfo<const LHCb::Track*>( key, def ) {}
// ============================================================================
// the specific printout
// ============================================================================
std::ostream& LoKi::Track::Info::fillStream( std::ostream& s ) const {
  return s << "TrINFO(" << index() << "," << value() << ")";
}
// ============================================================================
/*  constructor from the function, key and update-flag
 *  @param index the key in LHCb::Track::extraInfo table
 *  @param fun functionto be evaluated for missing keys
 *  @param update the flag to allow the insert of mnissing information
 */
// ============================================================================
LoKi::Track::SmartInfo::SmartInfo( const int index, const LoKi::BasicFunctors<const LHCb::Track*>::Function& fun,
                                   const bool update )
    : LoKi::AuxFunBase( std::tie( index, fun, update ) )
    , LoKi::ExtraInfo::GetSmartInfo<const LHCb::Track*>( index, fun, update ) {}
// ============================================================================
// the specific printout
// ============================================================================
std::ostream& LoKi::Track::SmartInfo::fillStream( std::ostream& s ) const {
  s << "TrSINFO(" << func() << "," << index() << ",";
  if ( update() ) {
    s << "True";
  } else {
    s << "False";
  }
  return s << ")";
}
// ============================================================================
// constructor with the state indicator
// ============================================================================
LoKi::Track::StateZ::StateZ( const LHCb::State::Location location )
    : LoKi::AuxFunBase( std::tie( location ) ), m_state( location ), m_bad( LoKi::Constants::InvalidDistance ) {}
// ============================================================================
// constructor with the state indicator
// ============================================================================
LoKi::Track::StateZ::StateZ( const LHCb::State::Location location, const double bad )
    : LoKi::AuxFunBase( std::tie( location, bad ) ), m_state( location ), m_bad( bad ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Track::StateZ* LoKi::Track::StateZ::clone() const { return new LoKi::Track::StateZ( *this ); }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::StateZ::result_type LoKi::Track::StateZ::operator()( LoKi::Track::StateZ::argument t ) const {
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return 'bad'" );
    return m_bad;
  }
  /// get the state:
  const LHCb::State* s = t->stateAt( m_state );
  if ( 0 == s ) {
    Error( "There is no state at " + state() + ", return 'bad' " );
    return m_bad;
  }
  return s->z();
}
// ============================================================================
// get the string representation of the state
// ============================================================================
const std::string& LoKi::Track::StateZ::state() const {
  if ( !m__state.empty() ) { return m__state; }
  std::ostringstream ss;
  ss << m_state;
  m__state = ss.str();
  return m__state;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::StateZ::fillStream( std::ostream& s ) const {
  switch ( m_state ) {
  case LHCb::State::Location::FirstMeasurement:
    return s << " TrFIRSTHITZ ";
  default:
    break;
  }
  return s << " TrSTATEZ( LHCb.State." << state() << " ) ";
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::HasStateAt::HasStateAt( const LHCb::State::Location& loc )
    : LoKi::AuxFunBase( std::tie( loc ) ), m_loc( loc ) {}
// ============================================================================
// mandatory: the only one essential method
// ============================================================================
LoKi::Track::HasStateAt::result_type LoKi::Track::HasStateAt::operator()( LoKi::Track::HasStateAt::argument t ) const {
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return 'false'" );
    return false;
  }
  //
  return t->hasStateAt( m_loc );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::IsOnTrack::IsOnTrack( const LHCb::LHCbID& id ) : LoKi::AuxFunBase( std::tie( id ) ), m_id( id ) {}
// ============================================================================
// mandatory: the only one essential method
// ============================================================================
LoKi::Track::IsOnTrack::result_type LoKi::Track::IsOnTrack::operator()( LoKi::Track::HasStateAt::argument t ) const {
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return 'false'" );
    return false;
  }
  //
  return t->isOnTrack( m_id );
}

// ============================================================================
// constructor from indices
// ============================================================================
LoKi::Track::Cov2::Cov2( const unsigned short i, const unsigned short j )
    : LoKi::AuxFunBase( std::tie( i, j ) )
    , LoKi::BasicFunctors<const LHCb::Track*>::Function()
    , m_case( _First )
    , m_z( -1 * Gaudi::Units::km )
    , m_loc( LHCb::State::Location::FirstMeasurement )
    , m_i( i )
    , m_j( j ) {
  Assert( m_i < 5 && m_j < 5, "Invalid indices" );
}
// ============================================================================
// constructor from indices & state location
// ============================================================================
LoKi::Track::Cov2::Cov2( const LHCb::State::Location location, const unsigned short i, const unsigned short j )
    : LoKi::AuxFunBase( std::tie( location, i, j ) )
    , LoKi::BasicFunctors<const LHCb::Track*>::Function()
    , m_case( _Location )
    , m_z( -1 * Gaudi::Units::km )
    , m_loc( location )
    , m_i( i )
    , m_j( j ) {
  Assert( m_i < 5 && m_j < 5, "Invalid indices" );
}
// ============================================================================
// constructor from indices and Z-position:
// ============================================================================
LoKi::Track::Cov2::Cov2( const double z, const unsigned short i, const unsigned short j )
    : LoKi::AuxFunBase( std::tie( z, i, j ) )
    , LoKi::BasicFunctors<const LHCb::Track*>::Function()
    , m_case( _Z )
    , m_z( z )
    , m_loc( LHCb::State::Location::FirstMeasurement )
    , m_i( i )
    , m_j( j ) {
  Assert( m_i < 5 && m_j < 5, "Invalid indices" );
}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Track::Cov2* LoKi::Track::Cov2::clone() const { return new LoKi::Track::Cov2( *this ); }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::Cov2::result_type LoKi::Track::Cov2::operator()( LoKi::Track::Cov2::argument t ) const {
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return NegativeInfnity" );
    return LoKi::Constants::NegativeInfinity;
  }
  //
  const LHCb::State* state = 0;
  switch ( m_case ) {
  case _Z:
    state = &( t->closestState( m_z ) );
    break;
  case _Location:
    state = t->stateAt( m_loc );
    break;
  default:
    state = &t->firstState();
  }
  //
  if ( 0 == state ) {
    Warning( "LHCb::State* points to NULL, use 'FirstState'" );
    state = &t->firstState();
  }
  //
  if ( 0 == state ) {
    Error( "LHCb::State* points to NULL, return 'NegativeInfinity'" );
    return LoKi::Constants::NegativeInfinity;
  }
  //
  return state->covariance()( m_i, m_j );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::Cov2::fillStream( std::ostream& s ) const {
  s << "TrCOV2(";
  //
  switch ( m_case ) {
  case _Z:
    s << m_z << ",";
    break;
  case _Location:
    s << "LHCb.State." << m_loc << ",";
    break;
  default:
    s << "";
  }
  //
  return s << m_i << "," << m_j << ")";
}

namespace LoKi::Track {
  // ============================================================================
  // MinimalImpactParameterChi2 functor
  // ============================================================================
  MinimalImpactParameterChi2::MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm, const std::string& location )
      : LoKi::AuxFunBase( std::tie( algorithm, location ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, location ) {}

  MinimalImpactParameterChi2::MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm )
      : LoKi::AuxFunBase( std::tie( algorithm ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, LHCb::Event::v2::RecVertexLocation::Primary ) {}

  MinimalImpactParameterChi2::result_type MinimalImpactParameterChi2::operator()( argument track ) const {
    if ( UNLIKELY( track == nullptr ) ) { return std::numeric_limits<result_type>::max(); }

    auto const* state = track->stateAt( LHCb::State::ClosestToBeam );
    if ( UNLIKELY( state == nullptr ) ) { return std::numeric_limits<result_type>::max(); }

    auto vertices = get();
    if ( UNLIKELY( vertices == nullptr ) ) { return std::numeric_limits<result_type>::max(); }

    auto calc_ipchi2 = [state]( auto const& vertex ) {
      return LHCb::TrackVertexUtils::vertexChi2( *state, vertex.position(), vertex.covMatrix() );
    };

    return std::accumulate( std::begin( *vertices ), std::end( *vertices ), std::numeric_limits<result_type>::max(),
                            [&calc_ipchi2]( result_type ipchi2, auto const& vertex ) {
                              return std::min( ipchi2, calc_ipchi2( vertex ) );
                            } );
  }

  std::ostream& MinimalImpactParameterChi2::fillStream( std::ostream& s ) const {
    s << "TrMINIPCHI2( ";
    Gaudi::Utils::toStream( location(), s ); // this handles the quoting
    return s << " )";
  }

  // ============================================================================
  // MinimalImpactParameterChi2Cut functor
  // ============================================================================
  MinimalImpactParameterChi2Cut::MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut,
                                                                const std::string& location )
      : LoKi::AuxFunBase( std::tie( algorithm, location ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, location )
      , m_ipchi2_cut( ipchi2_cut ) {}

  MinimalImpactParameterChi2Cut::MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut )
      : LoKi::AuxFunBase( std::tie( algorithm ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, LHCb::Event::v2::RecVertexLocation::Primary )
      , m_ipchi2_cut( ipchi2_cut ) {}

  MinimalImpactParameterChi2Cut::result_type MinimalImpactParameterChi2Cut::operator()( argument track ) const {
    if ( UNLIKELY( track == nullptr ) ) { return std::numeric_limits<result_type>::max(); }

    auto const* state = track->stateAt( LHCb::State::ClosestToBeam );
    if ( UNLIKELY( state == nullptr ) ) { return std::numeric_limits<result_type>::max(); }

    auto vertices = get();
    if ( UNLIKELY( vertices == nullptr ) ) { return std::numeric_limits<result_type>::max(); }

    auto small_ipchi2 = [this, state]( auto const& vertex ) {
      return LHCb::TrackVertexUtils::vertexChi2( *state, vertex.position(), vertex.covMatrix() ) < m_ipchi2_cut;
    };

    return std::none_of( std::begin( *vertices ), std::end( *vertices ), small_ipchi2 );
  }

  std::ostream& MinimalImpactParameterChi2Cut::fillStream( std::ostream& s ) const {
    s << "TrMINIPCHI2CUT( " << m_ipchi2_cut << ", ";
    Gaudi::Utils::toStream( location(), s ); // this handles the quoting
    return s << " )";
  }
} // namespace LoKi::Track

// ============================================================================
namespace {
  // hashing object
  const std::hash<const void*> s_hash{};
} // namespace
// ============================================================================
// hash
// ============================================================================
std::size_t LoKi::Track::hash( const LHCb::Track* track ) { return 0 == track ? 0 : s_hash( track ); }

// ============================================================================
// The END
// ============================================================================
