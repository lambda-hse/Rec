
2018-12-07 Rec v30r2
===

This version uses Lbcom v30r2, LHCb v50r2, Gaudi v30r5 and LCG_94 with ROOT 6.14.04
<p>
This version is released on `master` branch.

Built relative to Rec v30r1, with the following changes:

### New features

- Loki for Track v2, !1285 (@sstahl)   
  Add a small set of LoKi functors using Track v2 needed for making more realistic selections.

- Add copyright statements, !1247 (@clemenci) [LBCORE-1619]  

- PrForwardTool: add momentum guided search window, !1239 (@jihu)   
  - Turn it on with `PrForwardTool().UseMomentumGuidedSearchWindow = True` (default `False`)    
  - PrVeloUT: add a fitter with Y offset correction
    To turn it with `PrVeloUT().FinalFit = True` (default `False`)  
  - TrackSys: add switch `UseMomentumGuidedSearchWindow` in the `ConfigHLT1` field.
    If turned on, it will set both `PrForwardTool().UseMomentumGuidedSearchWindow` and `PrVeloUT().FinalFit` to be true in the fast stage tracking.   

- Dump banks, hits and geometry to binary files, !1177 (@raaij), !1229, !1267 (@dovombru), !1274 (@graven), !1288 (@freiss)     
  Code to dump VP banks, UT banks, UT hits, UT geometry, FT hits, MuonCoords, MuonCommonHits and track MC info to a binary file format or ROOT files for consumption by the standalone GPU application.

- Added new dumpers based on PrTrackerDumper, !1310 (@gtuci)

- Add GPU input readme, !1297 (@dovombru)   

- Add package CaloFuture, !1209 (@jmarchan)   
  New CaloFuture package to be used for upgrade

### Enhancements

- Making PatPV3D a Transformer instead of MulitTransformerFilter, !1325, (@chasse)   

- Batch evaluation of Catboost in MuonID, !1301 (@nkazeev)    

- ParameterizedKalman: Access qOverP directly, !1292 (@pseyfert)   

- Adapted to RecVertex v2, !1287 (@sponce)   
  
- Improve PatAlgorithms vectorization support, !1278 (@graven)   

- Use Property constructor which accepts updateHandler, !1275 (@graven)   

- Port CaloFutureShowerOverlap to new framework, !1273, !1293, !1304, !1313 (@cmarinbe)   

- Avoid implicit float to double conversion in PrPixel package, !1322 (@chasse)    

- Prevent PrForwardTracking from using fake qoverP, !1252 (@jihu)   
  When the input tracks are from PrPixelTracking, or you don't want to use momentum estimate from input tracks, set `PrForwardTool().UseMomentumEstimate = False`  
    
- Add a default q/p to the PrPixel track states, !1246 (@cattanem)   
  Implements a default q/p in PrPixel track states, as was done in Run1/2 for Velo tracks, to avoid NaNs due to infinite momentum. See discussions in lhcb/LHCb!1553 and lhcb/Brunel!540.      
  Enabled by default, can be switched off with `PrPixelTracking().AddQoverP = false`. Default pT can be changed with  `PrPixelTracking().ptVelo`, default is 400 MeV.

- Port CaloEnergyForTrack to new framework and remove PRS/SPD related classes in CaloPIDs, !1243 (@zhxu)   

- Changes for calorimeters RawBank access, !1303 (@jmarchan)   
  Addition of CaloFutureRawToDigits and removal of old code, related to lhcb/LHCb!1629   

- Updated the beam hole in PrHybridSeeding, !1237 (@lohenry)   
  The PrHybridSeeding has a possibility (True by default) to remove by hand the beam hole in the search for (u,v) hits. This cut was implemented when that hole was circular. This MR proposes new options to change the beam hole size and shape.  
  `RemoveHole` has been left to True by default.

- Use new counters, !1233, !1242 (@sponce)   
  
- Add new input types to ChargedProtoANNPID to provide information on (x,y,z) positions at various positions along the track, !1232 (@jonrob)   

- Adapt pattern recognition to Track::v2 class, !1205, !1226 (@sstahl), !1296 (@sponce)   

- Support selections on contiguous containers (e.g. vector<v2 Track>...), !1281 (@olupton)

- Restore old performance for Velo tracking  using Phi-Sorted hits, !1207 (@rquaglia)   
  Addresses https://gitlab.cern.ch/lhcb/Rec/issues/12  

- Only search for additional hits in X layers if there is actually an empty one, !1191 (@decianm)   

- Update TrackSys package to swallow main switchess for the HLT1 throughput and physics scenarios, !1188 (@rquaglia)   
  Allows to configure directly via a Dictionary the main configuration of the HLT1 reconstruction sequence.   

- New paramKalman extrapolation through the magnet, !1178 (@sstemmle)   
  In addition, some other hard coded numbers are replaced by parameters and some small changes to other parametrizations were applied. Uses new parameters for FT6x2, see lhcb-datapkg/ParamFiles!15.

- add PrCheckEmpty FilterPredicate (end decision for the scheduler), !1170 (@nnolte)   
  Also added the possibility of a sharedobjectscontainer as input for the trackeventfitter (for the barrier)    

- Optimized PV3D, !1169 (@sponce)   
  
- Optimized VPClus and VSPClus, !1168 (@sponce)   

- Optimized PrStoreFTHit by caching the DeFTMat objects, !1167 (@sponce)   

- Rec/LoKiTrack - change a way how LoKi-functors get the context, !1162 (@ibelyaev)   
  see  lhcb/LHCb!1441

- Adapt to versioned Track and revert of changes in TDR track, !1148 (@sstahl)   

- update Calo/CaloPIDs algorithms to the new Gaudi framework (charged PIDs only), !1133 (@zhxu)   

- RICH Set segment photon energies using parameters tool, !1111 (@jonrob)   

- Big improvements to MuonMatchVeloUT algorithm, !1033 (@mramospe)   

- Added support for VeloUTMuonMatch reco sequence in TrackSys, !1305 (@sponce)

- Merge of TDR branch, aka code that would break master but is needed to run TDR tests, !826 (@sponce)   


### Thread safety

- Modernize PrTableForFunction and PrUTMagnetTool, !1182 (@graven)   


### Bug fixes

- Add fully qualified enum name needed by FunctorCache generation, !1334 (@cattanem)   

- Fix two memory leaks in ParameterizedKalman/src/CompareTracks.cpp, !1284 (@graven)  

- MuonMatch fix to the gcc8 seqfault, !1309 (@cprouve)   

- Fix untested StatusCodes in Muon Rec packages, !1308 (@cattanem)   

- Fixes to untested StatusCodes seen by Brunel tests, !1306 (@cattanem)   
  
- PatAlgorithms - Fix UB Sanitizer error in PatBBDTSeedClassifier, !1206 (@jonrob)   
- MuonChi2MatchTool: fix undefined behaviour due to number of regions, !1248 (@rvazquez)   

- PrVeloUT: small correction to straight line extrapolation formula, !1245 (@jihu)   

- TrackMonitors - Increase the size of some char buffers to avoid buffer overflows, !1228 (@jonrob)   

- ChargedProtoANNPID - Suppress json warnings, !1222 (@jonrob)   

- Add array index protection to avoid stack overflows, !1218 (@jonrob)   

- PrAlgorithms - Fix MVA leak, !1213 (@jonrob)   

- CaloTools - Fix trivial TMVA leaks, !1212 (@jonrob)   

- Fixed bug in PrHybridSeeding, !1210 (@sponce)   
  Stupid mistake leading to memory corruption and detected jointly by valgrind and the memory sanitizers in the nightlies.  

- TrackBestTrackCreator: Add missing default inititalisation of `TrackData` members, !1208 (@jonrob)   

- Fix bug for the bestScatter tolerance in Velo tracking, !1204 (@rquaglia)   

- Fixed bug in ForwardTracking leading to use of deleted memory, !1203 (@sponce)   

- Add a callback to CommonMuonTool, to update cached geometry when alignment changes, !1201 (@cattanem) [LHCBPS-1800]  
  Cherry-picked from !1154 on `run2-patches`  
    

- Rich fix SIMD pixel summary ranges, !1194 (@jonrob)   
  Fixes a bug in the determination of the RICH SIMD pixel summary indices that occurs when no RICH1 bottom pixels are present.  

- Add missing .size() inside an assert() to fix compilation of the debug builds, !1186 (@olupton)   

- Fix floating point exception in PrCounter2, !1173 (@chasse)   
  This protects the PrCounter2 against a floating point exception in the printing.

- Fix assert bug., !1172 (@raaij)   
  This assert does not make sense and seems to always fail in the debug builds.  

- Fixes in TrackerDumper and PrLHCbID2MCParticle.cpp, !1171 (@dovombru)   
  Makes the TrackerDumper compatible with the latest changes.  

- Fixed mistake in StatZTraj on horizontal_and, !1152 (@sponce)   

- Fixed clang compilation error in VPClus, !1282 (@sponce)   

### Code modernisations and cleanups

- Backward compatible changes required to decommision COOL, !1324 (@clemenci)   

- Port CaloFutureMergedPi0 algorithm to the Gaudi::Functional framework, !1283 (@dgolubko)    

- Streamline LoKiTrack, !1315 (@graven)    
  Simplify definition of unary functions    

- Moved VectorSOAMatrixView header from the LHCb project to Rec, !1318 (@sponce)   

- Fix order of addition of converters to the sequence in TrackSys, !1316 (@sponce)

- Minor fixes to avoid gcc8 warnings, !1225 (@jonrob)   

- fix odr violations, !1277, !1289 (@graven)   
  Use constexpr and C++17 inline variables to fix violations of the [One Definition Rule](https://en.cppreference.com/w/cpp/language/definition)

- Prefer copy constructor over LHCb::State::clone, !1284 (@graven)   

- Use more direct way of obtaining the TES location of Calo hypos, !1312 (@graven)   

- Add TrMINIPCHI2 and TrMINIPCHI2CUT functors from Phys!455, !1307 (@olupton)   

- Fix -Wnon-virtual-dtor warning, !1295 (@graven)   

- merge PrForwardTool and PrForwardTracking, !1286 (@olupton)   

- return std::optional instead of boost::optional in ScalarTransformer call operator, !1279 (@graven)   

- replace ranges::v3::any with std::any, !1276 (@graven)   

- Remove UpgradeBestTrackCreator, !1266 (@cattanem)   

- tweak TrackInterfaces, TrackTools, !1263 (@graven)   
  * use ToolHandle  
  * inherit from extend instead of base_class/GaudiTool  
  * prefer struct for interfaces

- Modernize PatForward, !1262 (@graven)   
  * replace AUTO_RETURN macro with C++14 decltype(auto) return value deduction  
  * replace make_... functions with  class template argument deduction  
  * replace plain owning pointers with std::unique_ptr  
  * use std::invoke to generically invoke callables  
  * pick SIMD vector size depending on __AVX512F__, __AVX__ and __SSE__ pre-processer flags  
  * use std::inner_product instead of explicitly writing out an unrolled inner product  
  * replace custom index_sequence implementation with std::index_sequence

- use class template argument deduction, !1261 (@graven)   
  

- fully qualify enums, !1260 (@graven)   
  

- Modernize PrGeometryTool / PrForwardTool, !1255 (@graven)   
  * make methods const  
  * prefer Gaudi::Property over declareProperty  
  * prefer std::array properties over std::vector properties  
  * remove unused properties  
  * use ToolHandles  
  * prefer free-standing functions in anonymous namespace over member functions

- prefer SynchronizedValue over raw mutex, !1254 (@graven)   
  
- Remove obsolete RecSys/cmt/requirements and RecSys/CMakeLists.txt, !1250 (@cattanem)   

- Simplify vectorized code, !1244 (@graven)   
  By using generic lambdas instead of explicit templates

- PrCheckEmptyFilter.cpp: removed the info from execute(), and other minor changes, !1200 (@nnolte)   
  
- ChargedProtoANNPID - Disable instrumentation for some compilation units, !1199 (@jonrob)   

- Refactor UT code out of ST libraries, !1189 (@abeiter), !1235 (@sponce)   
  Classes updated to use new UT libraries in LHCb and Lbcom when handling UT code.

- Modernize PrVeloUT, PrUTMagnetTool, PrTableForFunction, !1185 (@graven)   
  * prefer std::clamp(x,y,z) over std::min(std::max(x,y),z),  
  * prefer constexpr  
  * prefer static functions  
  * prefer inherited constructor  
  * prefer template deduction guides  
  * prefer LHCb::span over std::array as function argument  
  * prefer std::array over C-style array  
  * static_assert over assert

- Make PrVeloUT less dependendant on actual container types, !1180 (@graven)   
  
- Do not to use std::make_pair unnecessarily, !1179 (@graven)   
  
- IMuonMatchTool: remove data from interface, move to implementation, !1176 (@graven)   

- prefer structured bindings over std::get<N>(), !1175 (@graven)   
  

- prefer template deduction guide over LHCb::make_array, !1174 (@graven)   
  
- Rich - C++17 improvements + clang formatting., !1160, !1181 (@jonrob)   

- deprecate use of getIterator_{Begin,End}, !1156 (@graven)   

- use structured bindings in combination with zip, !1149 (@graven)   
  
- cleanup of some Trajectory using code, !1146 (@graven)   
  - include the correct header files  
  - remove old WIN32 specific #ifdef  
  - add `final` where appropriate  
  - remove redundant destructor declarations

- Adressed several comments summarized in issue #25, !1145 (@sponce)   

- Modernize PrLongLivedTracking, !1144 (@graven)   
  - add additional argument to Transformer instead of using an  
    explicit DataObjectReadHandle  
  - prefer range-based for loops  
  - initialize member variables when they are declared  
  - move stand-alone struct into anonymous namespace  
  - fix one-time only memory leak by using std::unique_ptr  
  - avoid repeated push-back  
  - remove redundant inline qualifiers

- prefer std::string_view over boost::string_ref, !1143 (@graven)   

- prefer std::optional over boost::optional, !1142 (@graven)   

- Reduce the use of #ifdef in PrVeloUT, !1141 (@graven)   
  - instead, use templates and `if constexpr`  
  - replace explicit `DataObjectReadhandle` with extra `Transformer` argument  
  - use std::optional instead of boost::optional

- Minimize the ICaloClusterization interface, !1137 (@graven)   
  Follow changes in lhcb/LHCb!1431

- prefer CaloCluster copy constructor over virtual clone, !1135 (@graven)   


### Monitoring changes

- MuMonitor: Remove unnecessary get of HltDecReports, !1202 (@cattanem)   
  MuMonitor was retrieving `HltDecReports` from the TES, even though it was not using them. This MR removes it, allowing MuMonitor to work also on data taken in HLT passthrough mode where the HltDecReports are absent. See discussion in lhcb/Brunel!484  

- PrChecker histograms: plot negative eta range, !1251 (@dovombru)   
  For Velo track histograms of reconstructed and reconstructible tracks: plot the whole eta range: -7, 7 if no eta25 cut was specified.

- Convert SIMDRecoStats (Rich monitoring algorithm in Brunel) to use new Gaudi counters, !1220 (@jonrob)   

- Fix RecMoniConf to enable Monitoring of new Upgrade sub-detectors, !1249 (@cattanem)   

- Added back counters to HLT1 sequence, !1236 (@sponce)   

- Update PrChecking, !1223 (@rquaglia)   

- Explicit electron checks and efficiency of first velo hits in PrChecker2/PrCounter2, !1140 (@chasse) [N-3]  


### Changes to tests

- LumiAlgs - Disable virtual memory check under sanitizers, !1215 (@jonrob)   
  
- Fix test using the SQLite DDDB, broken by lhcb/LHCb!1439, !1150 (@cattanem)   
  Cherry picked from lhcb/Rec!1147 on `run2-patches`

- Remove assert to fix tests in dbg mode, !1139 (@rvazquez)   
  Brunel tests in debug mode were failing due to the assert in `CommonMuonStation.cpp` removed by this MR. See discussion in lhcb/Rec!1076.


  
