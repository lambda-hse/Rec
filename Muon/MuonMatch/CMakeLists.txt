###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: MuonMatch
################################################################################
gaudi_subdir(MuonMatch v1r0)

gaudi_depends_on_subdirs(Det/MuonDet
			 Associators/AssociatorsBase
                         Event/TrackEvent
			 GaudiAlg
			 Pr/PrKernel)

find_package(Boost)
find_package(ROOT COMPONENTS Hist Gpad Graf Matrix)
find_package(VDT)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(MuonMatch
                 src/*.cpp
                 INCLUDE_DIRS ROOT Associators/AssociatorsBase Tr/TrackInterfaces VDT
                 LINK_LIBRARIES ROOT MuonDetLib LinkerEvent MCEvent RecEvent TrackEvent PrKernel MuonInterfacesLib TrackFitEvent GaudiAlgLib RelationsLib MuonDAQLib)

gaudi_install_headers(MuonMatch)

gaudi_install_python_modules()
