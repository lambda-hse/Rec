/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCHPERFORMANCE_H
#define MUONMATCHPERFORMANCE_H 1

// STL
#include <string>
#include <vector>

// from Gaudi
#include "GaudiAlg/Consumer.h"

// from LHCb
#include "Associators/InputLinks.h"
#include "Associators/Location.h"
#include "Event/MCParticle.h"
#include "Linker/LinkerWithKey.h"

// from Local
#include "MuonMatch/Tracks_v2.h"

namespace MuonMatch {
  namespace tmp {
    using Track  = LHCb::Track;
    using Tracks = LHCb::Tracks;
  } // namespace tmp
} // namespace MuonMatch

namespace {
  using namespace MuonMatch;

  using TrackToMCParticlesLinker = InputLinks<tmp::Track, LHCb::MCParticle>;
} // namespace

/** @class MuonMatchPerformance MuonMatchPerformance.h
 *
 *  @author Miguel Ramos Pernas
 *  @date   2018-02-01
 */
class MuonMatchPerformance final
    : public Gaudi::Functional::Consumer<void( const tmp::Tracks&, const LHCb::LinksByKey& )> {

public:
  /// Build the class
  MuonMatchPerformance( const std::string& name, ISvcLocator* pSvcLocator );

  /// Execute the algorithm, filling the output tuple
  void operator()( const tmp::Tracks&, const LHCb::LinksByKey& ) const override;

protected:
  Gaudi::Property<std::vector<int>> m_daughterPIDs{
      this,
      "DaughterPIDs",
      {-13, +13},
      "IDs of the daughter particles to do the matching. No particles means that no matching is performed."};

  Gaudi::Property<std::vector<int>> m_skipMotherPIDs{
      this, "SkipMotherPIDs", {}, "IDs of the mother particles to skip during MC-matching."};

  Gaudi::Property<float> m_minMCMatchWeight{
      this,
      "MinMCMatchWeight",
      0.7,
      "Minimum weight for the linker in order to consider it a correctly matched particle or a ghost",
  };

  Gaudi::Property<bool> m_acceptanceCuts{
      this, "AcceptanceCuts", false,
      "Whether to apply acceptance cuts on the MC-matched particle associated to each track"};

  /// Counter for input tracks
  mutable Gaudi::Accumulators::StatCounter<> m_inputTracks{this, "Input tracks"};

  /// Counter for ghost tracks (tracks with no related MC-particle)
  mutable Gaudi::Accumulators::StatCounter<> m_pureGhostTracks{this, "Ghost tracks (no related particles)"};

  /// Counter for ghost tracks (those which do not satisfy the minimum weight)
  mutable Gaudi::Accumulators::StatCounter<> m_ghostTracks{this, "Ghost tracks (weight cut)"};

  /// Counter for matched daughters
  mutable Gaudi::Accumulators::StatCounter<> m_matchedDaughters{this, "Matched particles"};

  /// Counter for matched daughters coming directly from a beauty hadron
  mutable Gaudi::Accumulators::StatCounter<> m_directlyFromBeauty{this, "Matched directly from beauty"};
  /// Counter for matched daughters where at least one of the ancestors is a beauty hadron
  mutable Gaudi::Accumulators::StatCounter<> m_fromBeauty{this, "Matched from beauty"};

  /// Counter for matched daughters coming directly from a charm hadron
  mutable Gaudi::Accumulators::StatCounter<> m_directlyFromCharm{this, "Matched directly from charm"};
  /// Counter for matched daughters where at least one of the ancestors is a charm hadron
  mutable Gaudi::Accumulators::StatCounter<> m_fromCharm{this, "Matched from charm"};

  /// Counter for matched daughters coming directly from a strange hadron
  mutable Gaudi::Accumulators::StatCounter<> m_directlyFromStrange{this, "Matched directly from strange"};
  /// Counter for matched daughters where at least one of the ancestors is a strange hadron
  mutable Gaudi::Accumulators::StatCounter<> m_fromStrange{this, "Matched from strange"};

  /// Counter for particles skipped by mother PID constraints
  mutable Gaudi::Accumulators::StatCounter<> m_fromSkipped{this, "Skipped by mother PID"};

  /// Counter for particles with undefined parent
  mutable Gaudi::Accumulators::StatCounter<> m_fromUndefined{this, "Undefined"};
};

#endif // MUONMATCHPERFORMANCE_H
