/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCH_VELOUT_H
#define MUONMATCH_VELOUT_H 1

// STL
#include <string>
#include <tuple>
#include <utility>
#include <vector>

// boost
#include <boost/container/static_vector.hpp>

// from Gaudi
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// from LHCb
#include "Kernel/ILHCbMagnetSvc.h"

// from MuonDAQ
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonDAQ/MuonHitContainer.h"

// from MuonMatch
#include "MuonMatch/HitMatching.h"
#include "MuonMatch/Tracks.h"

namespace {
  using namespace MuonMatch;
} // namespace

/** @class MuonMatchVeloUT MuonMatchVeloUT.h
 *
 *  @author Roel Aaij, Vasileios Syropoulos
 *  @date   2010-12-02
 *
 *  @author Miguel Ramos Pernas
 *  @date   2017-10-05
 */

class MuonMatchVeloUT
    : public Gaudi::Functional::Transformer<Tracks( const TrackSelection&, const MuonHitContainer& )> {

public:
  using value_type    = double;
  using hit_type      = Hit<value_type>;
  using hits_type     = Hits<value_type>;
  using window_bounds = std::tuple<value_type, value_type, value_type, value_type>;

  /// Constructor
  MuonMatchVeloUT( const std::string& name, ISvcLocator* pSvcLocator );

  /// Initialize the instance
  StatusCode initialize() override {

    // Initialize transformer
    auto sc = Transformer::initialize();
    if ( sc.isFailure() ) return sc;

    // Initialize magnetic field service
    m_fieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

    return StatusCode::SUCCESS;
  }

  /// Match the given tracks to the seed
  std::optional<Track> match( const Track& seed, const MuonHitContainer& hit_cont ) const;

  /// Gaudi-functional method
  Tracks operator()( const TrackSelection& seeds, const MuonHitContainer& hit_cont ) const override {

    if ( seeds.empty() ) return {};

    m_seedCount += seeds.size();

    Tracks matched;

    for ( const auto& seed : seeds ) {

      auto t = this->match( seed, hit_cont );

      if ( t ) matched.emplace_back( std::move( *t ) );
    }

    m_matchCount += matched.size();

    return matched;
  }

private:
  /// Magnetic field service
  ILHCbMagnetSvc* m_fieldSvc = nullptr;

  /// Counter for the number of input seeds
  mutable Gaudi::Accumulators::AveragingCounter<> m_seedCount{this, "#seeds"};

  /// Counter for the matched tracks
  mutable Gaudi::Accumulators::AveragingCounter<> m_matchCount{this, "#matched"};

  // Values to use in the "kick" method
  Gaudi::Property<value_type> m_kickOffset{this, "KickOffset", 338.92 * Gaudi::Units::MeV};
  Gaudi::Property<value_type> m_kickScale{this, "KickScale", 1218.62 * Gaudi::Units::MeV};

  // Whether to set the value of q/p of the track after matching
  Gaudi::Property<bool> m_setQOverP{this, "SetQOverP", false};

  // Properties of the magnet focal plane
  Gaudi::Property<value_type> m_za{this, "MagnetPlaneParA", +5.331 * Gaudi::Units::m};
  Gaudi::Property<value_type> m_zb{this, "MagnetPlaneParB", -0.958 * Gaudi::Units::m};

  // Definition of the search windows for each muon station
  Gaudi::Property<std::array<std::pair<value_type, value_type>, 4>> m_window{
      this,
      "SearchWindows",
      {std::pair<value_type, value_type>{500 * Gaudi::Units::mm, 400 * Gaudi::Units::mm},
       std::pair<value_type, value_type>{600 * Gaudi::Units::mm, 500 * Gaudi::Units::mm},
       std::pair<value_type, value_type>{700 * Gaudi::Units::mm, 600 * Gaudi::Units::mm},
       std::pair<value_type, value_type>{800 * Gaudi::Units::mm, 700 * Gaudi::Units::mm}}};

  // General properties
  Gaudi::Property<value_type> m_maxChi2DoF{this, "MaxChi2DoF", 20};
  Gaudi::Property<bool>       m_fitY{this, "FitY", false};

  /// Get the first estimation of the magnet focal plane position from "tx2".
  inline hit_type magnetFocalPlaneFromTx2( const LHCb::State& state ) const {

    const auto z = m_za + m_zb * state.tx() * state.tx();

    return hit_type{state, z};
  }

  /// Add hits to the given candidate and return whether it can be fitted
  inline boost::container::static_vector<MuonChamber, 3> afterKickMuChs( const TrackType& tt ) const {
    // When modifying these lines, please also consider modifying the method
    // "firstMuCh" accordingly.
    switch ( tt ) {
    case LowP:
      return {M2};

    case MediumP:
      return {M3, M2};

    case HighP:
      return {M4, M3, M2};

    default:
      return {};
    }
  }

  /// Calculate the slope using the minimum momentum
  inline auto dtx( const Track& track ) const { return m_kickScale / ( track.p() - m_kickOffset ); }

  /// Add hits to the given candidate and return whether it can be fitted
  std::optional<hit_type> findHit( const MuonChamber& much, const LHCb::State& state, const hit_type& magnet_hit,
                                   const MuonHitContainer& hit_cont, const value_type slope ) const;

  /// First muon station for a given track type.
  inline boost::container::static_vector<MuonChamber, 2> firstMuCh( const TrackType& tt ) const {
    switch ( tt ) {
    case LowP:
      return {M3};

    case MediumP:
      return {M5, M4};

    case HighP:
      return {M5};

    default:
      return {};
    }
  }

  /** Calculate the window to search in the first station.
   *  Returns "xmin", "xmax", "ymin", "ymax".
   */
  window_bounds firstStationWindow( const Track& track, const LHCb::State& state, const CommonMuonStation& station,
                                    const hit_type& hit ) const;

  /// Fit the given hits, returning the slope of the fitted track, momentum and chi2/ndof
  std::tuple<value_type, value_type, value_type> fitHits( const hits_type& hits, const hit_type& magnetHit,
                                                          const LHCb::State& state ) const;

  /// Get the momentum given the slope in "x"
  inline auto momentum( value_type dtx ) const { return m_kickScale / std::fabs( dtx ) + m_kickOffset; }

  /** Calculate the window to search in the given station.
   *  Not to be applied in the first station.
   *  Returns "xmin", "xmax", "ymin", "ymax".
   */
  window_bounds stationWindow( const LHCb::State& state, const CommonMuonStation& station, const hit_type& magnet_hit,
                               const value_type slope ) const;
};

#endif // MUONMATCH_VELOUT_H
