/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/MuonTileID.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/IMuonFastPosTool.h" // Interface

/** @class MuonDetPosTool MuonDetPosTool.h component/MuonDetPosTool.h
 *
 *
 *  @author Giacomo GRAZIANI
 *  @date   2009-03-17
 */
class MuonDetPosTool : public extends<GaudiTool, IMuonFastPosTool> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override {
    StatusCode sc = GaudiTool::initialize();
    if ( !sc ) return sc;
    m_muonDetector = getDet<DeMuonDetector>( DeMuonLocation::Default );
    if ( !m_muonDetector ) {
      err() << "error retrieving the Muon detector element " << endmsg;
      return StatusCode::FAILURE;
    }
    return sc;
  }
  StatusCode calcTilePos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay, double& z,
                          double& deltaz ) const override {
    return m_muonDetector->Tile2XYZ( tile, x, deltax, y, deltay, z, deltaz );
  }

  StatusCode calcStripXPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay,
                            double& z, double& deltaz ) const override {
    return m_muonDetector->Tile2XYZ( tile, x, deltax, y, deltay, z, deltaz );
  }

  StatusCode calcStripYPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay,
                            double& z, double& deltaz ) const override {
    return m_muonDetector->Tile2XYZ( tile, x, deltax, y, deltay, z, deltaz );
  }

private:
  DeMuonDetector* m_muonDetector = nullptr;
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( MuonDetPosTool )
