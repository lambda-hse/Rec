/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MuonCoord.h"
#include "GaudiAlg/Transformer.h"
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonDAQ/CommonMuonStation.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/IMuonFastPosTool.h"

#include "boost/container/static_vector.hpp"
#include "boost/numeric/conversion/cast.hpp"
#include <algorithm>
#include <vector>

/** @class MuonCoordsToHits MuonCoordsToHits.h
 *  Used to be CommonMuonHitManager.
 *
 *  @author Roel Aaij
 *  @author Kevin Dungs
 *  @date   2015-01-03
 */
class MuonCoordsToHits final
    : public Gaudi::Functional::Transformer<MuonHitContainer( const std::vector<LHCb::MuonCoord>& coords )> {
public:
  MuonCoordsToHits( const std::string& name, ISvcLocator* pSvcLocator );

  MuonHitContainer operator()( const std::vector<LHCb::MuonCoord>& coords ) const override;

  StatusCode initialize() override;

private:
  // Data members
  DeMuonDetector*   m_muonDet     = nullptr;
  IMuonFastPosTool* m_muonPosTool = nullptr;
  size_t            m_nStations;

  mutable Gaudi::Accumulators::BinomialCounter<> m_badTile{this, "Impossible MuonTileID"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MuonCoordsToHits )

MuonCoordsToHits::MuonCoordsToHits( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"CoordLocation", LHCb::MuonCoordLocation::MuonCoords},
                   KeyValue{"Output", MuonHitContainerLocation::Default} ) {}

//=============================================================================
// Initialization
//=============================================================================

StatusCode MuonCoordsToHits::initialize() {
  StatusCode sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;

  m_muonDet = getDet<DeMuonDetector>( DeMuonLocation::Default );

  m_nStations = boost::numeric_cast<size_t>( m_muonDet->stations() );
  assert( m_nStations <= 5 );

  m_muonPosTool = tool<IMuonFastPosTool>( "MuonFastPosTool" );
  return sc;
}

MuonHitContainer MuonCoordsToHits::operator()( const std::vector<LHCb::MuonCoord>& muonCoords ) const {
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "==> Execute" << endmsg;
    debug() << "Detector version used: " << m_muonDet->version() << endmsg;
  }
  boost::container::static_vector<int, 5> nCoords( m_nStations, 0 );
  for ( const auto& coord : muonCoords ) ++nCoords[coord.key().station()];
  boost::container::static_vector<CommonMuonHits, 5> hits( m_nStations );
  for ( auto&& [h, n] : Gaudi::Functional::details::zip::range( hits, nCoords ) ) h.reserve( n );

  auto badTile = m_badTile.buffer();
  for ( const auto& coord : muonCoords ) {
    MuonLayout   layoutOne;
    unsigned int x1 = m_muonDet->getLayoutX( 0, coord.key().station(), coord.key().region() );
    unsigned int y1 = m_muonDet->getLayoutY( 0, coord.key().station(), coord.key().region() );
    layoutOne       = MuonLayout( x1, y1 );
    double     x = 0., dx = 0., y = 0., dy = 0., z = 0., dz = 0.;
    StatusCode sc;
    // for uncrossed hits
    if ( coord.uncrossed() ) {
      if ( coord.key().station() > ( m_nStations - 3 ) && coord.key().region() == 0 ) {
        sc = m_muonPosTool->calcTilePos( coord.key(), x, dx, y, dy, z, dz );
      } else {
        if ( coord.key().layout() == layoutOne ) {
          sc = m_muonPosTool->calcStripXPos( coord.key(), x, dx, y, dy, z, dz );
        } else {
          sc = m_muonPosTool->calcStripYPos( coord.key(), x, dx, y, dy, z, dz );
        }
      }
      // for crossed hits
    } else {
      sc = m_muonPosTool->calcTilePos( coord.key(), x, dx, y, dy, z, dz );
    }
    badTile += sc.isFailure();
    if ( sc.isFailure() ) {
      Warning( "Impossible MuonTileID" ).ignore();
      continue;
    }
    hits[coord.key().station()].emplace_back( coord.key(), x, dx, y, dy, z, dz, coord.uncrossed(), coord.digitTDC1(),
                                              coord.digitTDC1() - coord.digitTDC2() );
  }

  std::array<CommonMuonStation, 5> stations;
  // create a MuonHitContainer to be returned and stored in the TES
  // the s_xRegions property has all regions starting from M1.
  // For the upgrade nStations == 4, and M1 has been removed, so use
  // 1-5, for Run II use 0-5
  for ( unsigned int s = 0; s != hits.size(); ++s ) {
    stations[s] = CommonMuonStation{*m_muonDet, s, std::move( hits[s] )};
  }
  return MuonHitContainer{std::move( stations )};
}
