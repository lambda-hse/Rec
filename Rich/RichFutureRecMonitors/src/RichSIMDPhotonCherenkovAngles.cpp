/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <algorithm>
#include <sstream>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

// Track selector
#include "TrackInterfaces/ITrackSelector.h"

// RichDet
#include "RichDet/DeRichSystem.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonCherenkovAngles final : public Consumer<void( const LHCb::Track::Selection&,             //
                                                                const Summary::Track::Vector&,             //
                                                                const Relations::PhotonToParents::Vector&, //
                                                                const LHCb::RichTrackSegment::Vector&,     //
                                                                const CherenkovAngles::Vector&,            //
                                                                const SIMDCherenkovPhoton::Vector& ),
                                                          Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDPhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true );
    }

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Selection&             tracks,        //
                     const Summary::Track::Vector&             sumTracks,     //
                     const Relations::PhotonToParents::Vector& photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&     segments,      //
                     const CherenkovAngles::Vector&            expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&        photons ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// Get the per PD resolution histogram ID
    inline std::string pdResPlotID( const LHCb::RichSmartID hpd ) const {
      const Rich::DAQ::PDIdentifier hid( hpd );
      std::ostringstream            id;
      id << "PDs/pd-" << hid.number();
      return id.str();
    }

  private:
    // cached data

    // CK theta histograms
    RadiatorArray<AIDA::IHistogram1D*>             h_thetaRec         = {{}};
    RadiatorArray<AIDA::IHistogram1D*>             h_phiRec           = {{}};
    RadiatorArray<AIDA::IHistogram1D*>             h_ckResAll         = {{}};
    RadiatorArray<PanelArray<AIDA::IHistogram1D*>> h_ckResAllPerPanel = {{}};

  private:
    // properties and tools

    /// Which radiators to monitor
    Gaudi::Property<RadiatorArray<bool>> m_rads{this, "Radiators", {false, true, true}};

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.9999f, 0.9999f, 0.9999f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.030f, 0.010f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.060f, 0.036f}};

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<RadiatorArray<float>> m_ckResRange{this, "CKResHistoRange", {0.025f, 0.005f, 0.0025f}};

    /// Enable the per PD resolution plots, for the given radiators
    Gaudi::Property<RadiatorArray<bool>> m_pdResPlots{this, "EnablePerPDPlots", {false, false, false}};

    /** Track selector.
     *  Longer term should get rid of this and pre-filter the input data instead.
     */
    ToolHandle<const ITrackSelector> m_tkSel{this, "TrackSelector", "TrackSelector"};
  };

} // namespace Rich::Future::Rec::Moni

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------

StatusCode SIMDPhotonCherenkovAngles::prebookHistograms() {

  bool ok = true;

  // load Rich System det. elem.
  const auto deRichSys = acquire<DeRichSystem>( detSvc(), DeRichLocations::RichSystem );

  // List of HPDs
  const auto& hpds = deRichSys->allPDRichSmartIDs();

  // Loop over radiators
  for ( const auto rad : Rich::radiators() ) {
    if ( m_rads[rad] ) {
      // Which RICH ?
      const auto rich = ( rad == Rich::Rich2Gas ? Rich::Rich2 : Rich::Rich1 );

      // inclusive plots
      ok &= saveAndCheck( h_thetaRec[rad],
                          richHisto1D( HID( "thetaRec", rad ), "Reconstructed Ch Theta - All photons",
                                       m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), "Cherenkov Theta / rad" ) );
      ok &= saveAndCheck( h_phiRec[rad], richHisto1D( HID( "phiRec", rad ), "Reconstructed Ch Phi - All photons", 0.0,
                                                      2.0 * Gaudi::Units::pi, nBins1D(), "Cherenkov Phi / rad" ) );
      ok &= saveAndCheck( h_ckResAll[rad],
                          richHisto1D( HID( "ckResAll", rad ), "Rec-Exp Cktheta - All photons", -m_ckResRange[rad],
                                       m_ckResRange[rad], nBins1D(), "delta(Cherenkov theta) / rad" ) );

      // loop over detector sides
      for ( const auto side : Rich::sides() ) {
        ok &= saveAndCheck( h_ckResAllPerPanel[rad][side],
                            richHisto1D( HID( "ckResAllPerPanel", side, rad ), "Rec-Exp Cktheta - All photons",
                                         -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),
                                         "delta(Cherenkov theta) / rad" ) );
      }

      // Enable per PD resolution plots ?
      if ( UNLIKELY( m_pdResPlots[rad] ) ) {
        // Loop over PDs
        for ( const auto PD : hpds ) {
          if ( PD.rich() != rich ) continue;
          // construct title
          const auto         copyN = deRichSys->copyNumber( PD );
          std::ostringstream title;
          title << "Rec-Exp Cktheta - All photons - PD#" << copyN << " " << PD;
          // construct ID
          const HID hID( pdResPlotID( PD ), rad );
          // book the plot
          ok &= ( nullptr != richHisto1D( hID, title.str(), -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),
                                          "delta(Cherenkov theta) / rad" ) );
        }
      }

    } // rad is active
  }   // rad loop

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void SIMDPhotonCherenkovAngles::operator()( const LHCb::Track::Selection&             tracks,        //
                                            const Summary::Track::Vector&             sumTracks,     //
                                            const Relations::PhotonToParents::Vector& photToSegPix,  //
                                            const LHCb::RichTrackSegment::Vector&     segments,      //
                                            const CherenkovAngles::Vector&            expTkCKThetas, //
                                            const SIMDCherenkovPhoton::Vector&        photons ) const {

  // loop over the track containers
  for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {
    // Is this track selected ?
    if ( !m_tkSel.get()->accept( *tk ) ) continue;

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {
      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];
      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !m_rads[rad] ) continue;

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // The PID type to assume. Just use Pion here.
      const auto pid = Rich::Pion;

      // beta
      const auto beta = richPartProps()->beta( pTot, pid );
      // selection cuts
      if ( beta < m_minBeta[rad] || beta > m_maxBeta[rad] ) continue;

      // expected CK theta
      const auto thetaExp = expCKangles[pid];

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( phot.validityMask()[i] ) {

          // reconstructed theta
          const auto thetaRec = phot.CherenkovTheta()[i];
          // reconstructed phi
          const auto phiRec = phot.CherenkovPhi()[i];
          // delta theta
          const auto deltaTheta = thetaRec - thetaExp;

          // SmartID
          const auto id = phot.smartID()[i];

          // Detctor side
          const auto side = id.panel();

          // fill some plots
          h_thetaRec[rad]->fill( thetaRec );
          h_phiRec[rad]->fill( phiRec );
          h_ckResAll[rad]->fill( deltaTheta );
          h_ckResAllPerPanel[rad][side]->fill( deltaTheta );

          // Per PD plots
          if ( UNLIKELY( m_pdResPlots[rad] ) ) {
            const auto pdID = id.pdID();
            auto       h    = richHisto1D( HID( pdResPlotID( pdID ), rad ) );
            if ( h ) { h->fill( deltaTheta ); }
          }

        } // valid scalars
      }   // SIMD loop
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonCherenkovAngles )

//-----------------------------------------------------------------------------
