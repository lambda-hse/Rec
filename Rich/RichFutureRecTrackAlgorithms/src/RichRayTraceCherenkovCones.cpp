/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <sstream>
#include <utility>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi
#include "GaudiAlg/Transformer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichFutureUtils/RichRayTracing.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichSIMDTypes.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Rec Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// DetDesc
#include "DetDesc/ConditionAccessorHolder.h"

// RichDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"

// VDT
#include "vdt/sincos.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class RayTraceCherenkovCones RichRayTraceCherenkovCones.h
   *
   *  Creates the Cherenkov cones for each segment and mass hypothesis,
   *  using photon raytracing.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class RayTraceCherenkovCones final : public Transformer<MassHypoRingsVector( const LHCb::RichTrackSegment::Vector&, //
                                                                               const CherenkovAngles::Vector&,        //
                                                                               const Utils::RichSmartIDs&,            //
                                                                               const Utils::RayTracing& ),
                                                          LHCb::DetDesc::usesBaseAndConditions<AlgBase<>,           //
                                                                                               Utils::RichSmartIDs, //
                                                                                               Utils::RayTracing>> {

  public:
    /// Standard constructor
    RayTraceCherenkovCones( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                        KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Emitted},
                        // input conditions data
                        KeyValue{"RichSmartIDs", Utils::RichSmartIDs::DefaultConditionKey},
                        KeyValue{"RichRayTracing", Utils::RayTracing::DefaultConditionKey}},
                       // data output
                       {KeyValue{"MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted}} ) {
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

    /// Initialization after creation
    StatusCode initialize() override;

  public:
    /// Functional operator
    MassHypoRingsVector operator()( const LHCb::RichTrackSegment::Vector& segments,       //
                                    const CherenkovAngles::Vector&        ckAngles,       //
                                    const Utils::RichSmartIDs&            smartIDsHelper, //
                                    const Utils::RayTracing&              rayTrace ) const override;

  private:
    // helper classes

    /** @class CosSinPhi RichRayTraceCherenkovCone.h
     *
     *  Utility class to cache cos and sin values
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   17/02/2008
     */
    template <typename TYPE>
    class CosSinPhi : public Vc::AlignedBase<Vc::VectorAlignment> {
    public:
      /// Container type
      using Vector = Rich::SIMD::STDVector<CosSinPhi<TYPE>>;

    public:
      /// Contructor from a phi value
      explicit CosSinPhi( const TYPE _phi ) : phi( _phi ) { Rich::Maths::fast_sincos( phi, sinPhi, cosPhi ); }

    public:
      TYPE phi    = 0; ///< CK phi
      TYPE cosPhi = 0; ///< Cos(CK phi)
      TYPE sinPhi = 0; ///< Sin(CK phi)
    };

  private:
    // SIMD types
    using FP         = Rich::SIMD::DefaultScalarFP;
    using SIMDFP     = SIMD::FP<FP>;
    using SIMDVector = SIMD::Vector<FP>;

  private:
    /// Bailout number
    RadiatorArray<unsigned int> m_nBailout = {{}};

    /// Cached SIMD cos and sin values around the ring, for each radiator
    RadiatorArray<CosSinPhi<SIMDFP>::Vector> m_cosSinPhiV;

    /// Cached trace modes for each radiator
    RadiatorArray<LHCb::RichTraceMode> m_traceModeRad = {{}};

  private:
    /// Number of points to ray trace on each ring, for each radiator
    Gaudi::Property<RadiatorArray<unsigned int>> m_nPoints{this, "NRingPoints", {96u, 96u, 96u}};

    /// Flag to turn on or off checking of intersections with beampipe
    Gaudi::Property<bool> m_checkBeamPipe{this, "CheckBeamPipe", false};

    /// Flag to switch between simple or detail HPD description in ray tracing
    Gaudi::Property<bool> m_useDetailedHPDsForRayT{this, "UseDetailedHPDs", false};

    /** Bailout fraction. If no ray tracings have worked after this
     *  fraction have been perfromed, then give up */
    Gaudi::Property<RadiatorArray<float>> m_bailoutFrac{this, "BailoutFraction", {0.75f, 0.75f, 0.75f}};
  };

} // namespace Rich::Future::Rec

//=============================================================================

using namespace Rich::Future::Rec;

//=============================================================================

StatusCode RayTraceCherenkovCones::initialize() {

  // Sets up various tools and services
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // loop over radiators
  for ( const auto rad : Rich::radiators() ) {

    // To simplify things demand an exact fit to SIMD vector size
    if ( m_nPoints[rad] % SIMDFP::Size != 0 ) {
      error() << rad << " nPoints (" << m_nPoints[rad] << ") not a multiple of SIMD vector size (" << SIMDFP::Size
              << ")." << endmsg;
      return StatusCode::FAILURE;
    }

    // Fill cos and sin values
    m_cosSinPhiV[rad].clear();
    m_cosSinPhiV[rad].reserve( m_nPoints[rad] / SIMDFP::Size );
    std::size_t ivc( 0 );
    SIMDFP      phiSIMD( 0 );
    const auto  incPhi = Gaudi::Units::twopi / static_cast<double>( m_nPoints[rad] );
    double      ckPhi  = 0.0;
    for ( unsigned int iPhot = 0; iPhot < m_nPoints[rad]; ++iPhot, ckPhi += incPhi ) {
      // Vc value
      ivc          = iPhot % SIMDFP::Size;
      phiSIMD[ivc] = ckPhi;
      // If SIMD value is full, push to vector
      if ( SIMDFP::Size - 1 == ivc ) {
        m_cosSinPhiV[rad].emplace_back( phiSIMD );
        phiSIMD = SIMDFP::Zero();
      }
    }

    // bailout number
    m_nBailout[rad] = static_cast<unsigned int>( m_bailoutFrac[rad] * m_nPoints[rad] );
  }

  // the ray-tracing mode
  LHCb::RichTraceMode tmpMode( LHCb::RichTraceMode::DetectorPlaneBoundary::RespectPDTubes,
                               ( m_useDetailedHPDsForRayT ? LHCb::RichTraceMode::DetectorPrecision::SphericalPDs
                                                          : LHCb::RichTraceMode::DetectorPrecision::FlatPDs ) );
  if ( m_checkBeamPipe ) { tmpMode.setBeamPipeIntersects( true ); }
  m_traceModeRad.fill( tmpMode );
  _ri_debug << "Rich1Gas Track " << m_traceModeRad[Rich::Rich1Gas] << endmsg;
  _ri_debug << "Rich2Gas Track " << m_traceModeRad[Rich::Rich2Gas] << endmsg;

  // Derived condition objects.
  Utils::RichSmartIDs::addConditionDerivation( this );
  Utils::RayTracing::addConditionDerivation( this );

  // return
  return sc;
}

//=============================================================================

MassHypoRingsVector                                                                       //
RayTraceCherenkovCones::operator()( const LHCb::RichTrackSegment::Vector& segments,       //
                                    const CherenkovAngles::Vector&        ckAngles,       //
                                    const Utils::RichSmartIDs&            smartIDsHelper, //
                                    const Utils::RayTracing&              rayTrace ) const {

  // The data to return
  MassHypoRingsVector ringsV;
  ringsV.reserve( segments.size() );

  // local position corrector
  // longer term need to remove this
  const Rich::Rec::RadPositionCorrector<SIMDFP> corrector;

  // Comparison result for good ray tracings
  const RayTracingUtils::SIMDResult::Results goodRes( (int)LHCb::RichTraceMode::RayTraceResult::InPDPanel );

  // Seems

  // loop over the input data
  for ( const auto&& [segment, ckTheta] : Ranges::ConstZip( segments, ckAngles ) ) {

    // Add a set of mass hypo rings for this segment
    ringsV.emplace_back();
    auto& rings = ringsV.back();

    // which radiator
    const auto rad = segment.radiator();

    // which rich
    const auto rich = segment.rich();

    // best emission point
    const auto& emissionPoint = segment.bestPoint();

    //_ri_vero << "segment " << emissionPoint << endmsg;

    // Loop over PID types
    for ( const auto id : activeParticlesNoBT() ) {
      // Above threshold ?
      if ( ckTheta[id] > 0 ) {

        //_ri_verbo << id << " " << ckTheta[id] << endmsg;

        // compute sin and cos theta
        SIMDFP sinTheta( SIMDFP::Zero() ), cosTheta( SIMDFP::Zero() );
        Maths::fast_sincos( SIMDFP( ckTheta[id] ), sinTheta, cosTheta );

        // reserve size in the points container
        rings[id].reserve( m_nPoints[rad] );

        // loop around the ring to create the directions
        SIMD::STDVector<SIMDVector> simdVects;
        simdVects.reserve( m_cosSinPhiV[rad].size() );
        for ( const auto& P : m_cosSinPhiV[rad] ) {
          // Photon direction around loop
          simdVects.emplace_back( segment.vectorAtCosSinThetaPhi( cosTheta, sinTheta, P.cosPhi, P.sinPhi ) );
        }

        // The vectorised ray tracing.
        // Move the directions as we do not care about them after.
        const auto results =
            rayTrace.traceToDetector( emissionPoint, std::move( simdVects ), segment, m_traceModeRad[rad] );

        // loop over the results and fill
        unsigned int nPhot( 0 ), nOK( 0 );
        for ( const auto&& [res, cosphi] : Ranges::ConstZip( results, m_cosSinPhiV[rad] ) ) {

          // Count total photons
          nPhot += SIMDFP::Size;

          // Count good photons
          nOK += ( res.result >= goodRes ).count();

          // bailout check
          if ( UNLIKELY( 0 == nOK && nPhot >= m_nBailout[rad] ) ) { break; }

          // detection point (SIMD)
          const auto& gP = res.detectionPoint;

          // get corrected SIMD local point
          const auto lP = corrector.correct( smartIDsHelper.globalToPDPanel( rich, gP ), rad );

          // Scalar loop to fill the output container
          // should eventually update output data model to keep in SIMD form
          GAUDI_LOOP_UNROLL( SIMDFP::Size )
          for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {

            // Add a new point
            rings[id].emplace_back( Gaudi::XYZPoint{gP.X()[i], gP.Y()[i], gP.Z()[i]},           //
                                    Gaudi::XYZPoint{lP.X()[i], lP.Y()[i], lP.Z()[i]},           //
                                    res.smartID[i],                                             //
                                    ( RayTracedCKRingPoint::Acceptance )( (int)res.result[i] ), //
                                    res.primaryMirror[i],                                       //
                                    res.secondaryMirror[i],                                     //
                                    res.photonDetector[i],                                      //
                                    cosphi.phi[i] );

            //_ri_verbo << std::setprecision(4)
            //          << LHCb::RichTraceMode::RayTraceResult(res.result[i]) << " " << gP <<
            //          endmsg;
            //_ri_verbo << rings[id].back() << endmsg;

          } // scalar loop

        } // results loop

        // if no good hits empty the container
        // if ( 0 == nOK ) { rings[id].clear(); }
      }
    }
  }

  return ringsV;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RayTraceCherenkovCones )

//=============================================================================
