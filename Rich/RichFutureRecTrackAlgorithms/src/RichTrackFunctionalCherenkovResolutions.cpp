/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <string>

// Gaudi Kernel
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/SystemOfUnits.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "RichFutureUtils/RichTabulatedRefIndex.h"
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichGeomFunctions.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Detector Description
#include "RichDet/DeRich1.h"

// DetDesc
#include "DetDesc/ConditionAccessorHolder.h"
#include "DetDesc/ITransportSvc.h"
#include "DetDesc/TransportSvcException.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {

    /// Data cache
    class TkResDataCache {
    public:
      /// Cache ref. index standard deviations for each radiator
      RadiatorArray<double> refIndexSD = {{0.488e-3, 0.2916e-4, 0.6777e-5}};

    public:
      /// Ban default constructor
      TkResDataCache() = delete;
      /// Constructor from a DeRich1
      TkResDataCache( const DeRich1& rich1 ) {
        // standard deviations
        // If we have HPDs change defaults ...
        const bool isPmt = ( rich1.RichPhotoDetConfig() == Rich::PMTConfig );
        if ( !isPmt ) { refIndexSD = {0.488e-3, 0.393e-4, 0.123e-4}; }
        // If the values exist in the DB, use these
        if ( rich1.exists( "RichBrunelRefIndexSDParameters" ) ) {
          const auto aRefSD = rich1.paramVect<double>( "RichBrunelRefIndexSDParameters" );
          refIndexSD        = {aRefSD[0], aRefSD[1], aRefSD[2]};
        }
      }
    };

  } // namespace

  /** @class TrackFunctionalCherenkovResolutions RichTrackFunctionalCherenkovResolutions.h
   *
   *  Computes the expected Cherenkov resolutions for the given track segments
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackFunctionalCherenkovResolutions final
      : public Transformer<CherenkovResolutions::Vector( const LHCb::RichTrackSegment::Vector&, //
                                                         const CherenkovAngles::Vector&,        //
                                                         const MassHypoRingsVector&,            //
                                                         const TkResDataCache&,                 //
                                                         const Utils::TabulatedRefIndex& ),
                           LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, TkResDataCache, Utils::TabulatedRefIndex>> {

  public:
    /// Standard constructor
    TrackFunctionalCherenkovResolutions( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data input
                       {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                        KeyValue{"SignalCherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                        KeyValue{"MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted},
                        // conditions input
                        KeyValue{"DataCache", name + "-DataCache"},
                        KeyValue{"TabulatedRefIndex", name + "-TabulatedRefIndex"}},
                       // data outputs
                       {KeyValue{"CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default}} ) {
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

    /// Initialization after creation
    StatusCode initialize() override;

  public:
    /// Algorithm execution via transform
    CherenkovResolutions::Vector operator()( const LHCb::RichTrackSegment::Vector& segments,  //
                                             const CherenkovAngles::Vector&        ckAngles,  //
                                             const MassHypoRingsVector&            massRings, //
                                             const TkResDataCache&                 dataCache, //
                                             const Utils::TabulatedRefIndex&       tabRefIndx //
                                             ) const override;

  private:
    // parameters

    /// Radiation length / unit length for each radiator
    RadiatorArray<double> m_radLenPerUnitL = {{0, 3.46137e-05, 1.14196e-05}};

    /// Scattering coefficent. should be used with p in GeV
    const double m_scatt = 13.6e-03;

  private:
    // services

    /// Transport Service
    ServiceHandle<ITransportSvc> m_transSvc{this, "TransportSvc", "TransportSvc"};

  private:
    // properties

    /// Overall scale factors for each radiator
    Gaudi::Property<RadiatorArray<float>> m_scale{this, "ScaleFactor", {1.0f, 1.0f, 1.0f}};

    /// Flag to turn on the full treatment of MS using the TS
    Gaudi::Property<RadiatorArray<bool>> m_useTSForMS{this, "UseTSForMultScat", {false, false, false}};

    /// Flag to turn on the caching of the radiation length parameter
    Gaudi::Property<RadiatorArray<bool>> m_cacheRadLenP{this, "CacheRadLenParam", {false, true, true}};

    /// RICH PD contributions to CK theta resolution
    Gaudi::Property<RadiatorArray<double>> m_pdErr{this, "PDErrors", {0.0005, 0.0002, 0.0002}};

    /// RICH PD reference areas (mm^2).
    Gaudi::Property<RadiatorArray<double>> m_pdRefArea{this, "PDRefAreas", {7.77016, 7.77016, 37.4688}};

    /// Flag to turn on the full PD area treatment per radiator
    Gaudi::Property<RadiatorArray<bool>> m_fullPDAreaTreatment{this, "FullPDAreaTreatment", {false, false, true}};

    /// Absolute max CK theta resolution per radiator
    Gaudi::Property<RadiatorArray<float>> m_maxRes{this, "MaxCKThetaRes", {0.003f, 0.0025f, 0.001f}};

  private:
    // messaging

    /// Problems during determination of path length
    mutable WarningCounter m_pathLenWarn{this, "Problem computing radiation length"};

    /// Null PD pointer
    mutable WarningCounter m_nullPD{this, "NULL DeRichPD pointer !!"};
  }; // namespace Rich::Future::Rec

} // namespace Rich::Future::Rec

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

StatusCode TrackFunctionalCherenkovResolutions::initialize() {

  // Sets up various tools and services
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // derived conditions
  addConditionDerivation<TkResDataCache( const DeRich1& )>( DeRichLocations::Rich1, inputLocation<TkResDataCache>() );
  Utils::TabulatedRefIndex::addConditionDerivation( this );

  // Compute for each radiator the radiator radiation length / unit path length

  // Create accelerator cache for the transport service
  auto tsCache = m_transSvc->createCache();

  // Fixed vector start/end points for each radiator
  const RadiatorArray<std::pair<Gaudi::XYZPoint, Gaudi::XYZPoint>> radVs{{
      {{150, 150, 1110}, {150, 150, 1170}},  // Aero (not used in practise)
      {{150, 150, 1500}, {150, 150, 1700}},  // RICH1 gas
      {{500, 500, 10000}, {500, 500, 11000}} // RICH2 gas
  }};
  // Use the fixed vectors to compute from the TS the radiation length / mm
  // in each RICH radiator medium
  for ( const auto rad : Rich::radiators() ) {
    if ( m_cacheRadLenP[rad] && !m_useTSForMS[rad] ) {
      const auto effL = m_transSvc->distanceInRadUnits_r( radVs[rad].first,  //
                                                          radVs[rad].second, //
                                                          tsCache, 0 );

      const auto length     = std::sqrt( ( radVs[rad].second - radVs[rad].first ).mag2() );
      m_radLenPerUnitL[rad] = ( length > 0 ? effL / length : 0.0 );
      _ri_debug << std::setprecision( 9 ) << rad //
                << " radiation length / mm = " << m_radLenPerUnitL[rad] << endmsg;
    }
  }

  // return
  return sc;
}

//=============================================================================

CherenkovResolutions::Vector                                                                      //
TrackFunctionalCherenkovResolutions::operator()( const LHCb::RichTrackSegment::Vector& segments,  //
                                                 const CherenkovAngles::Vector&        ckAngles,  //
                                                 const MassHypoRingsVector&            massRings, //
                                                 const TkResDataCache&                 dataCache, //
                                                 const Utils::TabulatedRefIndex&       tabRefIndx //
                                                 ) const {

  using namespace Gaudi::Units;

  // data to return
  CherenkovResolutions::Vector resV;
  resV.reserve( segments.size() );

  // Create accelerator cache for the transport service
  auto tsCache = m_transSvc->createCache();

  // Loop over input segment data
  for ( auto&& [segment, angles, rings] : Ranges::ConstZip( segments, ckAngles, massRings ) ) {

    // Add a resolution entry for this segment
    resV.emplace_back();
    auto& res = resV.back();

    // Is the lowest mass hypo for this track above threshold
    const bool aboveThreshold = ( angles[lightestActiveHypo()] > 0 );

    // momentum for this segment, in GeV units
    const auto ptot = segment.bestMomentumMag() / GeV;

    // check if segment is valid
    if ( aboveThreshold && ptot > 0 ) {

      // Which radiator
      const auto rad = segment.radiator();

      // cache 1 / momentum
      const auto ptotInv = 1.0 / ptot;

      // the res^2 to fill
      double res2 = 0;

      // Start with the mass hypo independent bits

      //-------------------------------------------------------------------------------
      // multiple scattering
      //-------------------------------------------------------------------------------
      double effL = 0;
      if ( LIKELY( !m_useTSForMS[rad] ) ) {
        // compute from the cached parameter
        effL = m_radLenPerUnitL[rad] * segment.pathLength();
      } else {
        // Full TS treatment ...
        try {
          effL = m_transSvc->distanceInRadUnits_r( segment.entryPoint(), segment.exitPoint(), //
                                                   tsCache, 0 );
        } catch ( const TransportSvcException& excpt ) {
          effL = 0;
          ++m_pathLenWarn;
          debug() << "Exception during path length determination " << excpt.message() << endmsg;
        }
      }
      // compute the scattering coefficient from the radiation length
      if ( effL > 0 ) {
        const auto multScattCoeff = ( m_scatt * std::sqrt( effL ) * ( 1.0 + 0.038 * Rich::Maths::fast_log( effL ) ) );
        const auto scatCOvP       = multScattCoeff * ptotInv;
        const auto scattErr       = 2.0 * scatCOvP * scatCOvP;
        //_ri_debug << std::setprecision(9) << " Scattering error = " << scattErr << endmsg;
        res2 += scattErr;
      }

      //-------------------------------------------------------------------------------
      // track curvature in the radiator volume
      //-------------------------------------------------------------------------------
      const auto curvErr = ( UNLIKELY( Rich::Aerogel == rad ) //
                                 ? 0
                                 : std::pow( Rich::Geom::AngleBetween( segment.entryMomentum(), //
                                                                       segment.exitMomentum() ) *
                                                 0.25,
                                             2 ) );
      //_ri_debug << std::setprecision(9) << " Curvature error = " << curvErr << endmsg;
      res2 += curvErr;

      //-------------------------------------------------------------------------------

      //-------------------------------------------------------------------------------
      // tracking direction errors
      //-------------------------------------------------------------------------------
      const auto dirErr = segment.entryErrors().errTX2() + segment.entryErrors().errTY2();
      //_ri_debug << std::setprecision(9) << " Track direction error = " << dirErr << endmsg;
      res2 += dirErr;
      //-------------------------------------------------------------------------------

      // Loop over active hypos ( note including BT here )
      for ( const auto hypo : activeParticles() ) {

        // start with the hypo independent part
        double hypo_res2 = res2;

        //-------------------------------------------------------------------------------
        // RICH contributions (pixel, PSF errors etc...)
        // Uses the supplied reference contribution for the given pixel area, and scales
        // according to the area for the associated PD
        //-------------------------------------------------------------------------------
        auto pdErr = m_pdErr[rad] * m_pdErr[rad];
        // Are we using the full treatment to deal with PD size difference ?
        if ( UNLIKELY( m_fullPDAreaTreatment[rad] && Rich::BelowThreshold != hypo ) ) {
          // loop over the ring points for this hypo
          unsigned int totalInPD{0};
          double       totalSize{0.0};
          if ( !rings[hypo].empty() ) {
            for ( const auto& P : rings[hypo] ) {
              if ( RayTracedCKRingPoint::InHPDTube == P.acceptance() ) {
                // Check PD pointer
                if ( P.photonDetector() ) {
                  // Count accepted points
                  ++totalInPD;
                  // sum up pixel size
                  totalSize += P.photonDetector()->effectivePixelArea();
                } else {
                  ++m_nullPD;
                }
              }
            }
            // Scale PD errors by average pixel size using reference size
            if ( totalInPD > 0 ) {
              totalSize /= (double)totalInPD;
              pdErr *= totalSize / m_pdRefArea[rad];
            }
          }
        }
        //_ri_debug << std::setprecision(9) << " HPD error = " << pdErr << endmsg;
        hypo_res2 += pdErr;

        // Expected CK theta
        if ( LIKELY( Rich::BelowThreshold != hypo && angles[hypo] > 1e-6 ) ) {

          // cache tan(cktheta)
          const auto tanCkExp = Rich::Maths::fast_tan( angles[hypo] );
          // 1 /  tan(cktheta)
          const auto tanCkExpInv = 1.0 / tanCkExp;
          //_ri_debug << std::setprecision(9) << "  " << hypo
          //          << " ckExp = " << angles[hypo]
          //          << " tanCkExp = " << tanCkExp << endmsg;

          //-------------------------------------------------------------------------------
          // chromatic error
          //-------------------------------------------------------------------------------
          const auto index        = tabRefIndx.refractiveIndex( segment.radIntersections(), segment.avPhotonEnergy() );
          const auto chromFact    = ( index > 0 ? dataCache.refIndexSD[rad] / index : 0.0 );
          const auto chromatErr   = chromFact * tanCkExpInv;
          const auto chromatErrSq = chromatErr * chromatErr;
          //_ri_debug << std::setprecision(9) << "         Chromatic err = " << chromatErr <<
          // endmsg;
          hypo_res2 += chromatErrSq;
          //-------------------------------------------------------------------------------

          //-------------------------------------------------------------------------------
          // momentum error
          //-------------------------------------------------------------------------------
          constexpr auto GeVSqInv   = 1.0 / (double)( GeV * GeV );
          const auto     mass2      = richPartProps()->massSq( hypo ) * GeVSqInv;
          const auto     massFactor = mass2 / ( mass2 + ( ptot * ptot ) );
          const auto     A          = massFactor * ptotInv * tanCkExpInv;
          const auto     momErrSq   = ( ( segment.entryErrors().errP2() * GeVSqInv ) * A * A );
          //_ri_debug << std::setprecision(9) << "         momentum err = " << momErrSq << endmsg;
          hypo_res2 += momErrSq;
          //-------------------------------------------------------------------------------

        } // theta > 0

        // Compute the final resolution, including global scale factor
        const auto ckRes = std::min( m_scale[rad] * (float)( std::sqrt( hypo_res2 ) ), m_maxRes[rad] );
        //_ri_debug << std::setprecision(9) << "           -> Final error^2 " << ckRes << endmsg;
        res.setData( hypo, ckRes );

      } // hypo loop

    } // above threshold

  } // track loop

  return resV;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackFunctionalCherenkovResolutions )

//=============================================================================
