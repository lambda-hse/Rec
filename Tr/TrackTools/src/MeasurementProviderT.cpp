/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class MeasurementProviderT MeasurementProviderT.cpp
 *
 * Implementation of templated MeasurementProvider tool
 * see interface header for description
 *
 *  @author W. Hulsbergen
 *  @date   07/06/2007
 */

#include "MeasurementProviderT.h"

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------
template <typename T>
StatusCode MeasurementProviderT<T>::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<typename T::DetectorType>( T::defaultDetectorLocation() );

  setProperty( "Tolerance", T::defaultTolerance() );
  return sc;
}

////////////////////////////////////////////////////////////////////////////////////////
// Template instantiations using Traits classes
////////////////////////////////////////////////////////////////////////////////////////

#include "TrackInterfaces/ISTClusterPosition.h"
#include "TrackInterfaces/IUTClusterPosition.h"
#include "TrackInterfaces/IVPClusterPosition.h"
#include "TrackInterfaces/IVeloClusterPosition.h"

#include "Kernel/LineTraj.h"

#include "Event/UTCluster.h"
#include "Event/UTLiteCluster.h"
#include "Event/VPCluster.h"
#include "Event/VPLightCluster.h"
#include "PrKernel/UTHitHandler.h"

#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTSensor.h"
#include "VPDet/DeVP.h"

namespace MeasurementProviderTypes {

  //////////// VP
  struct VP {
    using PositionToolType = IVPClusterPosition;
    static std::string positionToolName() { return "VPClusterPosition"; }

    using DetectorType = DeVP;
    static std::string const& defaultDetectorLocation() { return DeVPLocation::Default; }
    static double nominalZ( DetectorType const& det, LHCb::LHCbID id ) { return det.sensor( id.vpID() )->z(); }

    using Cluster = LHCb::VPLightCluster;

    using ClusterContainerType = LHCb::VPLightClusters;
    static std::string const& defaultClusterLocation() { return LHCb::VPClusterLocation::Light; }

    static LHCb::VPChannelID channelId( LHCb::LHCbID id ) { return id.vpID(); }

    static LHCb::Measurement makeMeasurement( LHCb::VPLightCluster const& clus, DeVP const& det,
                                              LHCb::VPPositionInfo info, bool localY ) {
      Gaudi::XYZPoint position( info.x, info.y, clus.z() );
      const auto*     sensor = det.sensor( clus.channelID().sensor() );
      if ( localY ) {
        return {clus.channelID(), clus.z(),
                LHCb::LineTraj<double>{position, LHCb::Trajectory<double>::Vector{1, 0, 0},
                                       LHCb::Trajectory<double>::Range{-info.dx, info.dx},
                                       LHCb::Trajectory<double>::DirNormalized{true}},
                info.dy, sensor};
      } else {
        return {clus.channelID(), clus.z(),
                LHCb::LineTraj<double>{position, LHCb::Trajectory<double>::Vector{0, 1, 0},
                                       LHCb::Trajectory<double>::Range{-info.dy, info.dy},
                                       LHCb::Trajectory<double>::DirNormalized{true}},
                info.dx, sensor};
      }
    }

    static constexpr double defaultTolerance() { return 0.0005 * Gaudi::Units::mm; }
  };

  struct UTLite {
    using PositionToolType = IUTClusterPosition;
    static std::string positionToolName() { return "UTOnlinePosition/UTLiteClusterPosition"; }
    using DetectorType = DeUTDetector;
    static std::string const& defaultDetectorLocation() { return DeUTDetLocation::UT; }
    using Cluster              = UT::Hit;
    using ClusterContainerType = UT::HitHandler;
    static std::string const& defaultClusterLocation() { return UT::Info::HitLocation; }

    static LHCb::UTChannelID channelId( LHCb::LHCbID id ) { return id.utID(); }

    static LHCb::Measurement makeMeasurement( UT::Hit const& clus, DeUTDetector const& det,
                                              IUTClusterPosition const& positiontool ) {
      const DeUTSector* utSector = det.findSector( clus.chanID() );
      return {clus.lhcbID(), utSector->globalCentre().z(), utSector->trajectory( clus.chanID(), clus.fracStrip() ),
              positiontool.error( clus.pseudoSize() ) * utSector->pitch(), utSector};
    }

    static constexpr double defaultTolerance() { return 0.002 * Gaudi::Units::mm; }
  };

} // namespace MeasurementProviderTypes

namespace {
  template <typename T>
  const auto* id2cluster( LHCb::LHCbID id, typename T::ClusterContainerType const& clusters ) {
    if constexpr ( std::is_same_v<T, MeasurementProviderTypes::VP> ) {
      const auto clus = LIKELY( id.isVP() )
                            ? std::find_if( clusters.begin(), clusters.end(),
                                            [cid = id.vpID().channelID()]( LHCb::VPLightCluster const& clus ) {
                                              return clus.channelID() == cid;
                                            } )
                            : clusters.end();
      return clus != clusters.end() ? &*clus : nullptr;
    } else if constexpr ( std::is_same_v<T, MeasurementProviderTypes::UTLite> ) {
      auto  cid  = id.utID();
      auto& hits = clusters.hits( cid.station(), cid.layer(), cid.detRegion(), cid.sector() );
      auto  hit = std::find_if( hits.begin(), hits.end(), [cid]( UT::Hit const& hit ) { return hit.chanID() == cid; } );
      return hit != hits.end() ? &*hit : nullptr;
    } else {
      return LIKELY( T::checkType( id ) ) ? clusters.object( T::channelId( id ) ) : nullptr;
    }
  }
} // namespace

//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------
template <typename T>
LHCb::Measurement MeasurementProviderT<T>::measurement( typename T::Cluster const& clus,
                                                        [[maybe_unused]] bool      localY ) const {
  if constexpr ( std::is_same_v<T, MeasurementProviderTypes::VP> ) {
    return T::makeMeasurement( clus, *m_det, m_positiontool->position( clus ), localY );
  } else if constexpr ( std::is_same_v<T, MeasurementProviderTypes::UTLite> ) {
    return T::makeMeasurement( clus, *m_det, *m_positiontool );
  }
}

//-----------------------------------------------------------------------------
/// Create a measurement with statevector. For now very inefficient.
//-----------------------------------------------------------------------------
template <typename T>
LHCb::Measurement MeasurementProviderT<T>::measurement( typename T::Cluster const&       clus,
                                                        LHCb::ZTrajectory<double> const& reftraj,
                                                        [[maybe_unused]] bool            localY ) const {
  if constexpr ( std::is_same_v<T, MeasurementProviderTypes::UTLite> ) { // ST & UTLite ignores reference
                                                                         // trajectory...
    return measurement( clus, localY );
  } else {
    if ( UNLIKELY( !m_useReference ) ) return measurement( clus, localY );
    LHCb::StateVector sv = reftraj.stateVector( T::nominalZ( *m_det, clus.channelID() ) );
    if constexpr ( std::is_same_v<T, MeasurementProviderTypes::VP> ) {
      return T::makeMeasurement( clus, *m_det, m_positiontool->position( clus, sv.position(), sv.tx(), sv.ty() ),
                                 localY );
    }
  }
}

//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

template <typename T>
void MeasurementProviderT<T>::addToMeasurements( LHCb::span<LHCb::LHCbID>         ids,
                                                 std::vector<LHCb::Measurement>&  measurements,
                                                 LHCb::ZTrajectory<double> const& reftraj ) const {
  measurements.reserve( measurements.size() + ids.size() );
  auto to_clus = [clusters = m_clustersDH.get()]( LHCb::LHCbID id ) { return id2cluster<T>( id, *clusters ); };
  std::transform( ids.begin(), ids.end(), std::back_inserter( measurements ), [&]( LHCb::LHCbID id ) {
    auto clus = to_clus( id );
    assert( clus != nullptr );
    return this->measurement( *clus, reftraj, false );
  } );
}

template <>
void MeasurementProviderT<MeasurementProviderTypes::VP>::addToMeasurements(
    LHCb::span<LHCb::LHCbID> ids, std::vector<LHCb::Measurement>& measurements,
    LHCb::ZTrajectory<double> const& ref ) const {
  measurements.reserve( measurements.size() + 2 * ids.size() );
  auto to_clus = [clusters = m_clustersDH.get()]( LHCb::LHCbID id ) {
    return id2cluster<MeasurementProviderTypes::VP>( id, *clusters );
  };
  std::for_each( ids.begin(), ids.end(), [&]( LHCb::LHCbID id ) {
    auto clus = to_clus( id );
    assert( clus != nullptr );
    measurements.push_back( measurement( *clus, ref, false ) );
    measurements.push_back( measurement( *clus, ref, true ) );
  } );
}

using VPMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::VP>;
DECLARE_COMPONENT( VPMeasurementProvider )
using UTLiteMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::UTLite>;
DECLARE_COMPONENT( UTLiteMeasurementProvider )
