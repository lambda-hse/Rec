/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackUsedLHCbID_H
#define _TrackUsedLHCbID_H

/** @class TrackUsedLHCbID TrackUsedLHCbID.h
 *
 * Implementation of TrackUsedLHCbID
 * check if an LHCbID is used
 *
 * @author M.Needham
 * @date   2/08/2006
 *
 * @author M. Schiller
 * @date 2015-02-21
 *  - use BloomFilters to achieve O(1) lookup instead of O(log(nHits))
 */

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "Kernel/IUsedLHCbID.h"

#include <cstdint>
#include <string>
#include <vector>

#include "Kernel/LHCbID.h"
#include "LHCbMath/BloomFilter.h"

struct ITrackSelector;

namespace TrackUsedLHCbIDImplDetail {
  /// calculate number of occupied channels with safety factor
  constexpr uint64_t occCh( uint64_t nch, uint64_t occnumer, uint64_t occdenom, uint64_t safety = 4 ) {
    return ( safety * nch * occnumer ) / occdenom;
  }
  /// calculate BloomFilter capacity for detector with given number of channels and occupancy
  constexpr uint64_t cap( uint64_t nch, uint64_t occnumer, uint64_t occdenom ) {
    return nch < occCh( nch, occnumer, occdenom ) ? nch : occCh( nch, occnumer, occdenom );
  }
} // namespace TrackUsedLHCbIDImplDetail

class TrackUsedLHCbID : public extends<GaudiTool, IUsedLHCbID, IIncidentListener> {
public:
  /** constructor */
  using extends::extends;

  /** intialize */
  StatusCode initialize() override;

  /** Test if the LHCbID is used
   * @param id to be test tested
   *  @return true if used
   */
  bool used( const LHCb::LHCbID id ) const override;

  /** Implement the handle method for the Incident service.
   *  This is used to nform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

private:
  void initEvent() const;

  typedef std::vector<std::string>     TrackContainers;
  typedef std::vector<ITrackSelector*> Selectors;
  /** Define containers and corresponding selectors in same order.
   *  E.g. inputContainers = "Rec/Track/Forward" and selectorNames = "ForwardSelector".
   */
  Gaudi::Property<TrackContainers> m_inputs{this, "inputContainers", {""}};
  // for track selection
  Gaudi::Property<std::vector<std::string>> m_names{this, "selectorNames", {""}};

  Selectors m_selectors;

  // set maximum number of hits expected in the BloomFilter:
  // if there's more hits in the detector than that, the number of collisions
  // (one hit mistaken for another) will rise above the threshold (1e-4)
  //
  // strategy: max(safety * maxocc * nChannels, nChannels) with safety = 4
  static constexpr uint64_t s_MaxVPHits    = TrackUsedLHCbIDImplDetail::cap( 4100000u, 125u, 100000u );
  static constexpr uint64_t s_MaxUTHits    = TrackUsedLHCbIDImplDetail::cap( 540000u, 18u, 1000u );
  static constexpr uint64_t s_MaxFTHits    = TrackUsedLHCbIDImplDetail::cap( 300000u, 2u, 100u );
  static constexpr uint64_t s_MaxOtherHits = 1024u; // up to 1024 which don't fit above
  static constexpr uint64_t s_denom        = 1u << 20u;
  static constexpr uint64_t s_numer        = 1u * s_denom / 10000u;

  /// flag bits
  enum { Initialized = 1, VP = 16, UT = 32, FT = 64, Other = 128 };
  mutable unsigned m_flags = 0; ///< flags
  // since current and Upgrade detectors are never used in the same job, we
  // can eke out some memory by putting corresponding detectors in a union
  // each, so they "share" the memory by overlapping in the same physical
  // memory location
  mutable BloomFilter<LHCb::LHCbID, s_MaxVPHits, s_numer, s_denom> m_vtx; ///< vertex detector hits
  mutable BloomFilter<LHCb::LHCbID, s_MaxUTHits, s_numer, s_denom> m_bmg; ///< hits in tracking detectors before the
                                                                          ///< magnet
  mutable BloomFilter<LHCb::LHCbID, s_MaxFTHits, s_numer, s_denom> m_amg; ///< hits in tracking detectors after the
                                                                          ///< magnet
  mutable BloomFilter<LHCb::LHCbID, s_MaxOtherHits, s_numer, s_denom> m_otherHits;
};

#endif
