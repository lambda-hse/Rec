/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Measurement.h"

#include "FTDet/DeFTMat.h"
#include "MuonDet/DeMuonChamber.h"
#include "UTDet/DeUTSector.h"
#include "VPDet/DeVPSensor.h"

namespace LHCb {

  const DetectorElement* Measurement::detectorElement() const {
    return visit( []( const FT& ft ) -> const DetectorElement* { return ft.mat; },
                  []( const Muon& m ) -> const DetectorElement* { return m.chamber; },
                  []( const UTLite& ut ) -> const DetectorElement* { return ut.sector; },
                  []( const VP& vp ) -> const DetectorElement* { return vp.sensor; } );
  }

} // namespace LHCb
