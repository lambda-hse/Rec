/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Include files
#include "Event/TrackTypes.h"
#include "Kernel/BrokenLineTrajectory.h"
#include "Kernel/CircleTraj.h"
#include "Kernel/LHCbID.h"
#include "Kernel/LineTraj.h"
#include "Kernel/PolymorphicValue.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/Trajectory.h"
#include "Kernel/meta_enum.h"
#include <variant>

class DetectorElement;

// VP
#include "Event/VPLightCluster.h"
class DeVPSensor;

// UT
#include "Event/UTHit.h"
class DeUTSector;

// FT
#include "Event/FTLiteCluster.h"
class DeFTMat;

// Muon
class DeMuonChamber;

namespace LHCb {
  /** @class Measurement Measurement.h
   *
   * Measurement defines the functionality required for the
   * Kalman Filter, Alignment and Monitoring
   *
   * @author Jose Hernando, Eduardo Rodrigues
   * created Fri Jan  4 20:26:46 2019
   *
   */
  namespace Enum::Measurement {
    /// enumerator for the type of Measurement
    meta_enum_class( Type, unsigned char, Unknown, Unknown2, Unknown3, Unknown4, Unknown5, Unknown6, Unknown7, Unknown8,
                     Muon, Unknown9, Unknown10, VP, Calo, Origin, FT, UT = 19, UTLite )
  } // namespace Enum::Measurement

  namespace details::Measurement {
    template <typename... T>
    struct overloaded : T... {
      using T::operator()...;
    };
    template <typename... T>
    overloaded( T... )->overloaded<T...>;

    template <typename Dir>
    bool isX( const Dir& dir ) {
      return dir.x() > dir.y();
    }

    template <typename T, typename Variant, std::size_t... I>
    constexpr int index_helper( std::index_sequence<I...> ) {
      auto b = std::array{std::is_same_v<T, std::variant_alternative_t<I, Variant>>...};
      for ( int i = 0; i != static_cast<int>( size( b ) ); ++i ) {
        if ( b[i] ) return i;
      }
      return -1;
    }

    template <typename T, typename Variant>
    constexpr int index() {
      constexpr auto idx = index_helper<T, Variant>( std::make_index_sequence<std::variant_size_v<Variant>>() );
      static_assert( idx != -1, "T does not appear in Variant" );
      return idx;
    }

  } // namespace details::Measurement

  class Measurement final {
  public:
    struct FT {
      LHCb::LineTraj<double> trajectory;
      const DeFTMat*         mat = nullptr;
    };
    struct Muon {
      LHCb::LineTraj<double> trajectory;
      const DeMuonChamber*   chamber = nullptr;
    };
    struct UT {
      LHCb::LineTraj<double> trajectory;
      const DeUTSector*      sector = nullptr;
    };
    struct UTLite : UT {};
    struct VP {
      /// x or y measurement
      enum class Projection { X = 1, Y = 2 };
      LHCb::LineTraj<double> trajectory;
      const DeVPSensor*      sensor = nullptr;
      Projection             projection() const {
        return details::Measurement::isX( trajectory.direction( 0 ) )
                   ? VP::Projection::Y // measurement is perpendicular to direction
                   : VP::Projection::X;
      }
#if defined( __GNUC__ ) && __GNUC__ < 8
      VP() noexcept {} // first element in variant, make default constructible..
      VP( LHCb::LineTraj<double> traj, const DeVPSensor* s ) noexcept : trajectory{std::move( traj )}, sensor( s ) {}
#endif
    };

  private:
    // Note: for now, use a variant. Alternatively, store this information 'elsewhere,
    //       and put a "pointer" (which could be "container + index" on top of SOA storage)
    //       to 'elsewhere' into the variant instead.
    using SubInfo = std::variant<VP, UTLite, FT, Muon>;

    double  m_z;          ///< the z-position of the measurement
    double  m_errMeasure; ///< the measurement error
    LHCbID  m_lhcbID;     ///< the corresponding LHCbID
    SubInfo m_sub;        ///< subdetector specific information

    template <typename SubI, typename = std::enable_if_t<std::is_convertible_v<SubI, SubInfo>>>
    Measurement( LHCbID id, double z, double errMeas, SubI&& subi )
        : m_z{z}, m_errMeasure{errMeas}, m_lhcbID{id}, m_sub{std::forward<SubI>( subi )} {}

  public:
    /// VP constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeVPSensor* elem )
        : Measurement{id, z, errMeas, VP{std::move( traj ), elem}} {}

    /// UTLite constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeUTSector* elem )
        : Measurement{id, z, errMeas, UTLite{{std::move( traj ), elem}}} {}

    /// FT constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeFTMat* elem )
        : Measurement{id, z, errMeas, FT{std::move( traj ), elem}} {}

    /// Muon constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeMuonChamber* elem )
        : Measurement{id, z, errMeas, Muon{std::move( traj ), elem}} {}

    // Observers

    /// invoke the callable which can be called for the detector-specific content in this measurement
    template <typename... Callables>
    decltype( auto ) visit( Callables&&... c ) const {
      return std::visit( details::Measurement::overloaded{std::forward<Callables>( c )...}, m_sub );
    }

    /// Get the sub-detector specific information
    template <typename SubI>
    const SubI* getIf() const;

    /// Check whether this Measurements 'is-a' specific 'sub' measurement
    template <typename SubI>
    bool is() const {
      return std::holds_alternative<SubI>( m_sub );
    }

    /// Retrieve the measurement error
    double resolution( const Gaudi::XYZPoint& /*point*/, const Gaudi::XYZVector& /*vec*/ ) const {
      return visit( [&]( const auto& ) { return errMeasure(); } );
    }

    /// Retrieve the measurement error squared
    double resolution2( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vec ) const {
      auto r = resolution( point, vec );
      return r * r;
    }

    /// Retrieve the trajectory representing the measurement
    Trajectory<double> const& trajectory() const {
      return visit( []( const auto& sub ) -> Trajectory<double> const& { return sub.trajectory; } );
    }

    /// Retrieve const  the corresponding LHCbID
    LHCbID lhcbID() const { return m_lhcbID; }

    /// Retrieve const  the z-position of the measurement
    double z() const { return m_z; }

    /// Retrieve const  the measurement error
    double errMeasure() const { return m_errMeasure; }

    /// Retrieve the corresponding detectorElement
    const DetectorElement* detectorElement() const;

    using Type = Enum::Measurement::Type;

    template <typename SubI>
    static constexpr auto index = details::Measurement::index<SubI, SubInfo>();

    /// Retrieve the measurement type
    Type type() const {
      auto idx = m_sub.index();
      ASSUME( idx < std::variant_size_v<SubInfo> );
      switch ( idx ) {
      case index<VP>:
        return Type::VP;
      case index<UTLite>:
        return Type::UTLite;
      case index<FT>:
        return Type::FT;
      case index<Muon>:
        return Type::Muon;
      default:
        return Type::Unknown;
      }
    };

    /// Modifiers

    template <typename SubI>
    SubI* getIf();

  }; //< class Measurement

  /// implementation of templated member functions requiring specialization

  template <typename SubI>
  inline const SubI* Measurement::getIf() const {
    if constexpr ( std::is_same_v<UT, SubI> ) {
      return getIf<UTLite>();
    } else {
      return std::get_if<SubI>( &m_sub );
    }
  }

  template <typename SubI>
  inline SubI* Measurement::getIf() {
    if constexpr ( std::is_same_v<UT, SubI> ) {
      return getIf<UTLite>();
    } else {
      return std::get_if<SubI>( &m_sub );
    }
  }

  template <>
  inline bool Measurement::is<Measurement::UT>() const {
    return is<UTLite>();
  }

} // namespace LHCb
