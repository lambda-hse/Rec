/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Scheduler.h"
#include "Types.h"
#include "VectorConfiguration.h"
#include <type_traits>

// ------------------
// Array constructors
// ------------------

namespace Tr {

  namespace TrackVectorFit {

    struct ArrayGenStates {

      template <size_t W, std::size_t... Is>
      static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 5 * W>
      InitialState_helper( const std::array<Sch::Item, W>& nodes, std::index_sequence<Is...> ) {
        return {nodes[Is].node->m_nodeParameters.m_referenceVector[0]...,
                nodes[Is].node->m_nodeParameters.m_referenceVector[1]...,
                nodes[Is].node->m_nodeParameters.m_referenceVector[2]...,
                nodes[Is].node->m_nodeParameters.m_referenceVector[3]...,
                nodes[Is].node->m_nodeParameters.m_referenceVector[4]...};
      }

      template <size_t W>
      static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 5 * W>
      InitialState( const std::array<Sch::Item, W>& nodes ) {
        return InitialState_helper<W>( nodes, std::make_index_sequence<W>{} );
      }
    };

  } // namespace TrackVectorFit

} // namespace Tr
