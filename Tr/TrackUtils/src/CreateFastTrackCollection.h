/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INCLUDE_CREATEFASTTRACKCOLLECTION_H
#define INCLUDE_CREATEFASTTRACKCOLLECTION_H 1

#include <string>
#include <vector>

#include "GaudiAlg/GaudiAlgorithm.h"

/** @class CreateFastTrackCollection CreateFastTrackCollection.h
 * given a list of input track containers, this algorithm creates a fast
 * GaudiSharedObjectsContainer containing pointers to the tracks in the
 * input containers given
 *
 * @author Manuel Tobias Schiller <schiller@physi.uni-heidelberg.de>
 * @date   2009-02-25
 */
class CreateFastTrackCollection : public GaudiAlgorithm {
public:
  /// Standard Constructor
  CreateFastTrackCollection( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm event execution

private:
  std::vector<std::string> m_inputLocations; ///< input locations
  std::string              m_outputLocation; ///< output location
  bool                     m_slowContainer;  ///< optionally deep-copy tracks into keyed cont.
};
#endif // INCLUDE_CREATEFASTTRACKCOLLECTION_H
