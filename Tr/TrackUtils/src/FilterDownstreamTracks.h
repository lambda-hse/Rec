/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FILTERDOWNSTREAMTRACKS_H
#define FILTERDOWNSTREAMTRACKS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class FilterDownstreamTracks FilterDownstreamTracks.h
 *  Filter Downstream tracks that share the T station part with a Forward
 *
 *  @author Olivier Callot
 *  @date   2010-06-15
 */
class FilterDownstreamTracks : public GaudiAlgorithm {
public:
  /// Standard constructor
  FilterDownstreamTracks( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  bool m_filter;
};
#endif // FILTERDOWNSTREAMTRACKS_H
