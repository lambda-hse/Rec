###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    GaudiSequencer, TrackMonitor, TrackVertexMonitor, TrackFitMatchMonitor,
    TrackV0Monitor, TrackDiMuonMonitor, TrackCaloMatchMonitor,
    TrackMuonMatchMonitor, TrackPV2HalfAlignMonitor, AlignmentOnlineMonitor)
from Configurables import (RecSysConf, RecMoniConf, TrackSys)


def ConfiguredTrackMonitorSequence(Name="TrackMonitorSequence",
                                   HistoPrint=False):

    # figure out detectors
    seq = GaudiSequencer(Name)
    subDets = None  #default, all monitors
    from Configurables import LHCbApp
    if hasattr(LHCbApp(), "Detectors"):
        if LHCbApp().isPropertySet("Detectors"):
            subDets = LHCbApp().getProp("Detectors")
    seq.Members.append(TrackMonitor(HistoPrint=HistoPrint))
    seq.Members.append(TrackDiMuonMonitor(HistoPrint=HistoPrint))
    seq.Members.append(TrackVertexMonitor(HistoPrint=HistoPrint))
    seq.Members.append(AlignmentOnlineMonitor(HistoPrint=HistoPrint))

    from Configurables import TrackSys
    if TrackSys().timing():
        seq.Members.append(TrackTimingMonitor(HistoPrint=HistoPrint))

    if not RecMoniConf().getProp("Histograms") is "Online":
        seq.Members.append(TrackV0Monitor(HistoPrint=HistoPrint))
        seq.Members.append(TrackFitMatchMonitor(HistoPrint=HistoPrint))
        seq.Members.append(TrackPV2HalfAlignMonitor(HistoPrint=HistoPrint))
        if "CALO" in RecSysConf().RecoSequence:
            if (subDets is None or "Ecal" in subDets):
                seq.Members.append(
                    TrackCaloMatchMonitor(
                        "TrackEcalMatchMonitor",
                        CaloSystem='Ecal',
                        HistoPrint=HistoPrint))
            #if ("Hcal" in subDets):
            #seq.Members.append(TrackCaloMatchMonitor("TrackHcalMatchMonitor", CaloSystem='Hcal', HistoPrint=HistoPrint))
            if (subDets is None or "Spd" in subDets):
                seq.Members.append(
                    TrackCaloMatchMonitor(
                        "TrackSpdMatchMonitor",
                        CaloSystem='Spd',
                        HistoPrint=HistoPrint))
            if (subDets is None or "Prs" in subDets):
                seq.Members.append(
                    TrackCaloMatchMonitor(
                        "TrackPrsMatchMonitor",
                        CaloSystem='Prs',
                        HistoPrint=HistoPrint))

        if "MUON" in RecSysConf().RecoSequence:
            if (subDets is None or "Muon" in subDets):
                seq.Members.append(
                    TrackMuonMatchMonitor(
                        "TrackMuonMatchMonitor", HistoPrint=HistoPrint))

    return seq
