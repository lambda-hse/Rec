/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================

#include "TrackTune.h"

#include "Event/Particle.h"
#include "Event/ProtoParticle.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

DECLARE_COMPONENT( TrackTune )

TrackTune::TrackTune( const std::string& name, ISvcLocator* pSvc ) : GaudiTupleAlg( name, pSvc ) {

  declareProperty( "ParticleLocation", m_particleLocation = "/Event/Dimuon/Phys/SelDiMuonInciLoose/Particles" );
  declareProperty( "TrackLocation", m_trackLocation = LHCb::TrackLocation::Default );
}

StatusCode TrackTune::initialize() {

  static const std::string histoDir = "Track/";
  if ( "" == histoTopDir() ) setHistoTopDir( histoDir );

  // Mandatory initialization of GaudiAlgorithm
  StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) { return sc; }

  LHCb::IParticlePropertySvc*   propertysvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  const LHCb::ParticleProperty* prop        = propertysvc->find( m_resonanceName );
  if ( !prop ) { return Error( "Failed to find resonance", StatusCode::SUCCESS ); }

  m_minMass = prop->mass() - m_deltaMass;
  m_maxMass = prop->mass() + m_deltaMass;
  propertysvc->release();

  info() << "MinMass " << m_minMass << " MaxMass " << m_maxMass << endmsg;

  return StatusCode::SUCCESS;
}

StatusCode TrackTune::execute() {

  // output tuple
  Tuple myTuple = nTuple( "Candidates" );

  const LHCb::Track::Range    tracks    = get<LHCb::Track::Range>( m_trackLocation );
  const LHCb::Particle::Range particles = get<LHCb::Particle::Range>( m_particleLocation );

  for ( const auto* t : select( particles ) ) {

    myTuple << Tuples::Column( "M", t->measuredMass() ) << Tuples::Column( "found", isFound( tracks, *t ) )
            << Tuples::Column( "PT", t->pt() ) << Tuples::Column( "Candidates", particles.size() );

    myTuple->write();
  }

  return StatusCode::SUCCESS;
  //
} // the end of the Algorihtm

const LHCb::Track* TrackTune::track( const LHCb::Particle& part ) const {
  const LHCb::ProtoParticle* proto = part.proto();
  return ( !proto || proto->charge() == 0 ) ? nullptr : proto->track();
}

bool TrackTune::isFound( const LHCb::Track::Range& tracks, const LHCb::Particle& part ) const {
  bool        ok        = true;
  const auto& daughters = part.daughters();
  for ( auto iter = daughters.begin(); iter != daughters.end() && ok; ++iter ) {
    const LHCb::Track* aTrack = track( **iter );
    if ( !aTrack ) info() << "Failed to find track " << endmsg;
    const double nHits        = aTrack->nLHCbIDs();
    bool         matchedTrack = false;
    for ( auto iterT = tracks.begin(); iterT != tracks.end() && !matchedTrack; ++iterT ) {
      const double fracCommon = aTrack->nCommonLhcbIDs( **iterT ) / double( nHits );
      plot( fracCommon, "purity", "purity", 0., 2., 100 );
      if ( fracCommon > m_minPurityCut ) matchedTrack = true;
    } // tracks
    if ( !matchedTrack ) ok = false;
  } // particles

  return ok;
}

std::vector<const LHCb::Particle*> TrackTune::select( const LHCb::Particle::Range& input ) const {
  if ( !m_selectBest ) return {input.begin(), input.end()};

  double                bestChi2 = 9999.;
  const LHCb::Particle* bestPart = nullptr;
  for ( const auto& i : input ) {
    if ( !inMassRange( *i ) ) continue;
    auto vert = i->endVertex();
    if ( vert->chi2PerDoF() < bestChi2 ) {
      bestChi2 = vert->chi2PerDoF();
      bestPart = i;
    }
  }
  if ( bestPart ) return {bestPart};
  return {};
}

bool TrackTune::inMassRange( const LHCb::Particle& particle ) const {
  double m = particle.measuredMass();
  return ( m > m_minMass && m < m_maxMass );
}
