/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <map>

// from Event
#include "Event/LinksByKey.h"
#include "Event/MCHeader.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/RecVertex.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Kernel/HitPattern.h"
#include "Kernel/LHCbID.h"

#include "TrackKernel/CubicStateInterpolationTraj.h"
#include "TrackKernel/TrackFunctors.h"
#include "TrackKernel/TrackVertexUtils.h"

// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/SystemOfUnits.h"

/** @class TrackIPResolutionCheckerBase
 *
 *  This base class provides the logic to create nTuples containing
 *  MC and reconstructed information related to the impact parameter
 *  and its calculation. Two versions exist of this algorithm,
 *  one which uses MCHits (and thus requires xdigi/xdst files),
 *  named TrackIPResolutionCheckerNTMCHits, and one which only
 *  requires links between the MCParticles and the tracks,
 *  called TrackIPResolutionCheckerNT.
 *
 *  @author W. Hulsbergen, P. Tsopelas, L.Dufour (modernisation only)
 */
template <bool useMCHits>
class TrackIPResolutionCheckerBase
    : public Gaudi::Functional::Consumer<
          std::conditional_t<useMCHits,
                             void( const LHCb::Track::Range&, const LHCb::MCParticles&, const LHCb::MCHeader&,
                                   const LHCb::LinksByKey&, const LHCb::RecVertices&, const LHCb::MCHits& ),
                             void( const LHCb::Track::Range&, const LHCb::MCParticles&, const LHCb::MCHeader&,
                                   const LHCb::LinksByKey&, const LHCb::RecVertices& )>,
          Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {

public:
  using TrackIPResolutionCheckerBase::Consumer::Consumer;

  StatusCode initialize() override;

protected:
  void fillTuple( const LHCb::Track::Range&, const LHCb::MCParticles&, const LHCb::MCHeader&, const LHCb::LinksByKey&,
                  const LHCb::RecVertices&, const LHCb::MCHits* hits ) const;

  typedef std::vector<const LHCb::MCHit*>                MCHitVector;
  typedef std::map<const LHCb::MCParticle*, MCHitVector> MCHitMap;
};

class TrackIPResolutionCheckerNT final : public TrackIPResolutionCheckerBase<false> {
public:
  TrackIPResolutionCheckerNT( const std::string& name, ISvcLocator* pSvcLocator )
      : TrackIPResolutionCheckerBase( name, pSvcLocator,
                                      {KeyValue{"TrackContainer", LHCb::TrackLocation::Velo},
                                       KeyValue{"MCParticleInput", LHCb::MCParticleLocation::Default},
                                       KeyValue{"MCHeaderLocation", LHCb::MCHeaderLocation::Default},
                                       KeyValue{"LinkerLocation", "Link/Pr/LHCbID"},
                                       KeyValue{"PVContainer", LHCb::RecVertexLocation::Primary}} ) {}
  void operator()( const LHCb::Track::Range& tracks, const LHCb::MCParticles& mcparticles,
                   const LHCb::MCHeader& mcheader, const LHCb::LinksByKey& linker,
                   const LHCb::RecVertices& pvs ) const override {
    return fillTuple( tracks, mcparticles, mcheader, linker, pvs, nullptr );
  }
};

class TrackIPResolutionCheckerNTMCHits final : public TrackIPResolutionCheckerBase<true> {
public:
  TrackIPResolutionCheckerNTMCHits( const std::string& name, ISvcLocator* pSvcLocator )
      : TrackIPResolutionCheckerBase( name, pSvcLocator,
                                      {
                                          KeyValue{"TrackContainer", LHCb::TrackLocation::Velo},
                                          KeyValue{"MCParticleInput", LHCb::MCParticleLocation::Default},
                                          KeyValue{"MCHeaderLocation", LHCb::MCHeaderLocation::Default},
                                          KeyValue{"LinkerLocation", "Link/Pr/LHCbID"},
                                          KeyValue{"PVContainer", LHCb::RecVertexLocation::Primary},
                                          KeyValue{"MCHitsLocation", "/Event/MC/Velo/Hits"},
                                      } ) {}

  void operator()( const LHCb::Track::Range& tracks, const LHCb::MCParticles& mcparticles,
                   const LHCb::MCHeader& mcheader, const LHCb::LinksByKey& linker, const LHCb::RecVertices& pvs,
                   const LHCb::MCHits& mchits ) const override {
    return fillTuple( tracks, mcparticles, mcheader, linker, pvs, &mchits );
  }
};

DECLARE_COMPONENT( TrackIPResolutionCheckerNTMCHits )
DECLARE_COMPONENT( TrackIPResolutionCheckerNT )

//=============================================================================
// Initialization. Check parameters
//=============================================================================
template <bool useMCHits>
StatusCode TrackIPResolutionCheckerBase<useMCHits>::initialize() {
  // Mandatory initialization of GaudiAlgorithm
  StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) { return sc; }

  // Can we still do this behaviour?
  // if ( m_linkerlocation.empty() ) m_linkerlocation = m_tracklocation;

  return StatusCode::SUCCESS;
}

namespace {
  const LHCb::MCVertex* findMCOriginVertex( const LHCb::MCParticle& particle, double decaylengthtolerance ) {
    // take this particle, walk up the decay tree and determine its originvertex
    const LHCb::MCVertex* originvertex = particle.originVertex();
    if ( originvertex ) {
      const LHCb::MCParticle* mother = originvertex->mother();
      if ( mother && mother != &particle ) {
        const LHCb::MCVertex* motheroriginvertex = mother->originVertex();
        if ( motheroriginvertex &&
             ( motheroriginvertex == originvertex ||
               ( ( motheroriginvertex->position() - originvertex->position() ).R() ) < decaylengthtolerance ) )
          originvertex = findMCOriginVertex( *mother, decaylengthtolerance );
      }
    }
    return originvertex;
  }

  int mcVertexType( const LHCb::MCVertex& vertex ) {
    int rc( -1 );
    if ( vertex.isPrimary() )
      rc = 0;
    else if ( vertex.mother() ) {
      const LHCb::MCParticle* mother = vertex.mother();
      if ( mother->particleID().hasBottom() && ( mother->particleID().isMeson() || mother->particleID().isBaryon() ) )
        rc = 3;
      else if ( mother->particleID().hasCharm() &&
                ( mother->particleID().isMeson() || mother->particleID().isBaryon() ) )
        rc = 2;
      else if ( mother->particleID().hasStrange() )
        rc = 1;
      else
        rc = 4;
    }
    return rc;
  }

  struct ForwardMCHitSorter {
    // copy constructor & destructor made by default
    ForwardMCHitSorter() {}
    bool operator()( const LHCb::MCHit* lhs, const LHCb::MCHit* rhs ) { return lhs->entry().z() < rhs->entry().z(); }
  };
  struct BackwardMCHitSorter {
    BackwardMCHitSorter() {}
    bool operator()( const LHCb::MCHit* lhs, const LHCb::MCHit* rhs ) { return rhs->entry().z() < lhs->entry().z(); }
  };

} // namespace

template <bool useMCHits>
void TrackIPResolutionCheckerBase<useMCHits>::fillTuple( const LHCb::Track::Range& tracks,
                                                         const LHCb::MCParticles&  mcparticles,
                                                         const LHCb::MCHeader& mcheader, const LHCb::LinksByKey& linker,
                                                         const LHCb::RecVertices& pvs,
                                                         const LHCb::MCHits*      hits ) const {
  // create the list of true PVs. count how many reconstructed tracks in each PV.
  std::map<const LHCb::MCVertex*, int> truepvs;
  // create a map from all MCParticles to MCHits
  MCHitMap mchitmap;

  if ( hits ) {
    // first collect
    for ( const LHCb::MCHit* mchit : *hits ) {
      if ( mchit->mcParticle() ) { mchitmap[mchit->mcParticle()].push_back( mchit ); }
    }
    // now sort them
    for ( MCHitMap::iterator it = mchitmap.begin(); it != mchitmap.end(); ++it ) {
      if ( it->first->momentum().Pz() > 0 )
        std::sort( it->second.begin(), it->second.end(), ForwardMCHitSorter() );
      else
        std::sort( it->second.begin(), it->second.end(), BackwardMCHitSorter() );
    }
  }

  auto theTuple = this->nTuple( "tracks", "", CLID_ColumnWiseTuple );

  int itrack = 0;

  // std::cout << "Number of tracks" << tracks.size() << std::endl ;
  for ( const LHCb::Track* track : tracks ) {
    // std::cout<< "a " << track << std::endl ;
    // keep track of track multiplicity
    theTuple->column( "itrack", int( itrack ) );
    ++itrack;
    theTuple->column( "ntrack", int( tracks.size() ) );
    theTuple->column( "numtruePV", int( mcheader.numOfPrimaryVertices() ) );

    if ( !track->hasVelo() ) continue;

    double x  = track->firstState().x();
    double y  = track->firstState().y();
    double z  = track->firstState().z();
    double tx = track->firstState().tx();
    double ty = track->firstState().ty();

    theTuple->column( "probChi2", track->probChi2() );
    theTuple->column( "chi2", track->chi2() );
    theTuple->column( "ndof", track->nDoF() );
    theTuple->column( "type", track->type() );
    theTuple->column( "x", x );
    theTuple->column( "y", y );
    theTuple->column( "z", z );
    theTuple->column( "tx", tx );
    theTuple->column( "ty", ty );
    theTuple->column( "qop", track->firstState().qOverP() );
    theTuple->column( "veloChi2", track->info( LHCb::Track::AdditionalInfo::FitVeloChi2, 0 ) );
    theTuple->column( "veloNdof", track->info( LHCb::Track::AdditionalInfo::FitVeloNDoF, 0 ) );
    theTuple->column( "TChi2", track->info( LHCb::Track::AdditionalInfo::FitTChi2, 0 ) );
    theTuple->column( "TNdof", track->info( LHCb::Track::AdditionalInfo::FitTNDoF, 0 ) );
    theTuple->column( "backward", track->checkFlag( LHCb::Track::Flags::Backward ) );

    const LHCb::State* stateAtFirstHit = track->stateAt( LHCb::State::Location::FirstMeasurement );
    theTuple->column( "firsthittx", double( stateAtFirstHit ? stateAtFirstHit->tx() : 0 ) );
    theTuple->column( "firsthitty", double( stateAtFirstHit ? stateAtFirstHit->ty() : 0 ) );
    theTuple->column( "firsthitx", double( stateAtFirstHit ? stateAtFirstHit->x() : 0 ) );
    theTuple->column( "firsthity", double( stateAtFirstHit ? stateAtFirstHit->y() : 0 ) );
    theTuple->column( "firsthitz", double( stateAtFirstHit ? stateAtFirstHit->z() : 0 ) );

    LHCb::HitPattern hitpattern( track->lhcbIDs() );
    theTuple->column( "numVeloStations", int( hitpattern.numVeloStations() ) );
    theTuple->column( "numVeloStationsOverlap", int( hitpattern.numVeloStationsOverlap() ) );
    theTuple->column( "numVeloHoles", int( hitpattern.numVeloHoles() ) );
    theTuple->column( "numTLayers", int( hitpattern.numTLayers() ) );
    theTuple->column( "numVeloStations", int( hitpattern.numVeloStations() ) );
    theTuple->column( "numVeloClusters", int( hitpattern.numVeloR() + hitpattern.numVeloPhi() ) );
    theTuple->column( "numhits", int( track->lhcbIDs().size() ) );

    const LHCb::State&                state = track->firstState();
    LHCb::CubicStateInterpolationTraj tracktraj( state, Gaudi::XYZVector() );
    Gaudi::XYZPoint                   trkpos( state.position() );
    Gaudi::XYZVector                  trkdir( state.slopes().Unit() );

    auto fit            = fitResult( *track );
    int  trackWasFitted = fit && !fit->nodes().empty();
    theTuple->column( "trackWasFitted", trackWasFitted );

    // We also want to monitor the reconstructed IP, so the IP with respect to the reconstructed PVs.
    theTuple->column( "numrecPV", int( pvs.size() ) );
    const LHCb::RecVertex* recpv( nullptr );
    double                 bestip2( 0 );

    for ( const LHCb::RecVertex* thispv : pvs ) {
      Gaudi::XYZVector dx    = trkpos - thispv->position();
      Gaudi::XYZVector delta = dx - trkdir * dx.Dot( trkdir );
      double           ip2   = delta.Mag2();
      if ( recpv == 0 || ip2 < bestip2 ) {
        bestip2 = ip2;
        recpv   = thispv;
      }
    }

    if ( recpv ) {
      LHCb::State stateAtVtx = tracktraj.state( recpv->position().z() );
      // now compute the errors. this isn't quite right because:
      // - PV is biased
      // - correction for Z error is approximate
      double tx        = stateAtVtx.tx();
      double recipxerr = std::sqrt( state.covariance()( 0, 0 ) + recpv->covMatrix()( 0, 0 ) +
                                    2 * tx * recpv->covMatrix()( 0, 2 ) + tx * tx * recpv->covMatrix()( 2, 2 ) );
      double ty        = stateAtVtx.ty();
      double recipyerr = std::sqrt( state.covariance()( 1, 1 ) + recpv->covMatrix()( 1, 1 ) +
                                    2 * ty * recpv->covMatrix()( 1, 2 ) + ty * ty * recpv->covMatrix()( 2, 2 ) );

      theTuple->column( "recIP3D", std::sqrt( bestip2 ) );
      theTuple->column( "recIPx", stateAtVtx.x() - recpv->position().x() );
      theTuple->column( "recIPy", stateAtVtx.y() - recpv->position().y() );
      theTuple->column( "recIPxerr", recipxerr );
      theTuple->column( "recIPyerr", recipyerr );

      auto bestipchi2 = LHCb::TrackVertexUtils::vertexChi2( stateAtVtx, recpv->position(), recpv->covMatrix() );
      theTuple->column( "recIPChi2", bestipchi2 );
    } // end of recpv check

    // now do things linked to MC truth
    const LHCb::MCParticle* mcparticle{nullptr};
    double                  maxWeight{0};
    linker.applyToLinks(
        track->key(), [&maxWeight, &mcparticle, &mcparticles]( unsigned int, unsigned int mcPartKey, float weight ) {
          if ( weight > maxWeight ) {
            maxWeight  = weight;
            mcparticle = static_cast<const LHCb::MCParticle*>( mcparticles.containedObject( mcPartKey ) );
          }
        } );

    bool hasMCMatch = mcparticle && mcparticle->originVertex();
    theTuple->column( "waslinked", hasMCMatch );

    // The "typeofprefix" tag is used to see which particles come from a True Primary Vertex,
    // not a True Primary vertex or whether they are ghosts.
    bool   isFromPV = hasMCMatch && mcparticle->originVertex()->isPrimary();
    double typeofprefix;
    typeofprefix = isFromPV ? 0 : ( hasMCMatch ? 1 : 2 );
    theTuple->column( "typeofprefix", typeofprefix );

    // std::cout<< "hasMCMatch: " << hasMCMatch << std::endl ;
    int mcOVT = -1; // set to -1 for ghosts
    if ( hasMCMatch ) {
      Gaudi::XYZPoint         trueorigin    = mcparticle->originVertex()->position();
      const LHCb::MCParticle* mother        = mcparticle->originVertex()->mother();
      const LHCb::MCVertex*   UltOrigVertex = findMCOriginVertex( *mcparticle, 1e-3 );
      const LHCb::MCParticle* ulmother      = UltOrigVertex ? UltOrigVertex->mother() : 0;
      mcOVT                                 = mcVertexType( UltOrigVertex );
      theTuple->column( "truepid", mcparticle->particleID().pid() );
      bool hasBottom = ulmother && ulmother->particleID().hasBottom() &&
                       ( ulmother->particleID().isMeson() || ulmother->particleID().isBaryon() );
      theTuple->column( "ulmotherHasBottom", hasBottom ? 1 : 0 );
      bool hasCharm = ulmother && ulmother->particleID().hasCharm() &&
                      ( ulmother->particleID().isMeson() || ulmother->particleID().isBaryon() );
      theTuple->column( "ulmotherHasCharm", hasCharm ? 1 : 0 );
      theTuple->column( "ulmotherHasStrange", ulmother && ulmother->particleID().hasStrange() ? 1 : 0 );
      theTuple->column( "truemotherpid", mother ? mother->particleID().pid() : 0 );
      theTuple->column( "truevertexpid", ulmother ? ulmother->particleID().pid() : 0 );
      theTuple->column( "truex", trueorigin.x() );
      theTuple->column( "truey", trueorigin.y() );
      theTuple->column( "truez", trueorigin.z() );
      theTuple->column( "truemom", mcparticle->p() / Gaudi::Units::GeV );
      theTuple->column( "truepx", mcparticle->momentum().Px() / Gaudi::Units::GeV );
      theTuple->column( "truepy", mcparticle->momentum().Py() / Gaudi::Units::GeV );
      theTuple->column( "truepz", mcparticle->momentum().Pz() / Gaudi::Units::GeV );
      theTuple->column( "truept", mcparticle->momentum().Pt() / Gaudi::Units::GeV );
      theTuple->column( "trueeta", mcparticle->momentum().eta() );
      theTuple->column( "truephi", mcparticle->momentum().phi() );

      // we can use the CubicStateInterpolationTraj (or later tracktraj)
      // to do the error propagation. it is just a line!
      LHCb::State state = tracktraj.state( trueorigin.z() );
      double      tx    = state.tx();
      double      ty    = state.ty();
      double      IPx   = state.x() - trueorigin.x();
      double      IPy   = state.y() - trueorigin.y();
      double      IP3D  = std::sqrt( ( IPx * IPx + IPy * IPy ) / ( 1 + tx * tx + ty * ty ) );
      theTuple->column( "IPx", IPx );
      theTuple->column( "IPy", IPy );
      theTuple->column( "IP3D", IP3D );
      theTuple->column( "IPxerr", std::sqrt( state.covariance()( 0, 0 ) ) );
      theTuple->column( "IPyerr", std::sqrt( state.covariance()( 1, 1 ) ) );

      // std::cout<< "hits: " << hits << " " << mcparticle << std::endl ;
      if ( hits ) {
        MCHitMap::const_iterator mchitmapit = mchitmap.find( mcparticle );
        if ( mchitmapit == mchitmap.end() ) {
          theTuple->column( "nummchits", int( 0 ) );
        } else {
          // store the z-position of the first MC hit
          const MCHitVector& mchits = mchitmapit->second;
          theTuple->column( "nummchits", int( mchits.size() ) );
          // std::cout<< "mchits: " << mchits.size() << std::endl ;

          const LHCb::MCHit* mchit  = mchits.front();
          const LHCb::MCHit* mchitL = mchits.back();
          Gaudi::XYZPoint    poshit = mchit->entry();
          theTuple->column( "edep", mchit->energy() );
          theTuple->column( "firstmchitx", poshit.x() );
          theTuple->column( "firstmchity", poshit.y() );
          theTuple->column( "firstmchitz", poshit.z() );
          theTuple->column( "firstmchitdz", mchit->displacement().z() );
          theTuple->column( "lastmchitz", mchitL->entry().z() );
          theTuple->column( "truetxfirstmchit", mchit->dxdz() );
          theTuple->column( "truetyfirstmchit", mchit->dydz() );
          double dz = poshit.z() - z;
          theTuple->column( "IPxfirstmchit", ( x + dz * tx ) - poshit.x() );
          theTuple->column( "IPyfirstmchit", ( y + dz * ty ) - poshit.y() );

          if ( stateAtFirstHit ) {
            // locate closest MCHit
            const LHCb::MCHit* closestmchit = mchit;
            for ( const LHCb::MCHit* anmchit : mchits ) {
              if ( std::abs( stateAtFirstHit->z() - anmchit->entry().z() ) <
                   std::abs( stateAtFirstHit->z() - mchit->entry().z() ) )
                mchit = anmchit;
            }
            theTuple->column( "truetxfirsthit", closestmchit->dxdz() );
            theTuple->column( "truetyfirsthit", closestmchit->dydz() );
            Gaudi::XYZPoint posmchit = closestmchit->entry();
            double          dz       = posmchit.z() - stateAtFirstHit->z();
            theTuple->column( "IPxfirsthit", ( stateAtFirstHit->x() + dz * stateAtFirstHit->tx() ) - posmchit.x() );
            theTuple->column( "IPyfirsthit", ( stateAtFirstHit->y() + dz * stateAtFirstHit->ty() ) - posmchit.y() );
          }

          // let's now extrapolate the mchit of the first hit to the z position of the vertex,
          // as if there were no scattering
          dz                        = trueorigin.z() - poshit.z();
          double extrapolatedmchitx = poshit.x() + dz * mchit->dxdz();
          double extrapolatedmchity = poshit.y() + dz * mchit->dydz();

          dz = trueorigin.z() - state.z();
          theTuple->column( "IPxfirsthitatvertex", ( state.x() + dz * state.tx() ) - extrapolatedmchitx );
          theTuple->column( "IPyfirsthitatvertex", ( state.y() + dz * state.ty() ) - extrapolatedmchity );
          // std::cout << "d" << std::endl ;
        } // numhits check
      }   // mchits exist check
    }     // end of MCMatch check

    theTuple->column( "mcOriginVertexType", mcOVT );

    theTuple->write();
  }
}
