/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TsaLine3D_H
#define _TsaLine3D_H

// from Kernel/LHCbDefinitions
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/Line.h"

// local
#include "TsaKernel/Line.h"

namespace Tf {
  namespace Tsa {

    /* @typedef for a line in 3-D
     *
     *  @author M.Needham
     *  @date   31/05/2004
     */
    typedef Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector> Line3D;

    /// Create a Line3D from a point line and z reference point
    Line3D createLine3D( const Tsa::Line& xLine, const Tsa::Line& yLine, const double zRef );

  } // namespace Tsa
} // namespace Tf

#endif
